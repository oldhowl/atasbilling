using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Interfaces.Services;
using Application.СQRS.Baskets.Queries.GetCheckoutPrice;
using Common;
using Common.ValueObjects;
using Domain.Abstractions;
using Domain.BoundedContexts.Baskets.Aggregate;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.BoundedContexts.Owner.Aggregate;
using Domain.BoundedContexts.Selling.Abstractions;
using Domain.BoundedContexts.Subscriptions.Aggregate;
using Domain.BoundedContexts.Subscriptions.Models;
using Domain.Enums;
using Domain.ValueObjects;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using Tests.Common;

namespace Tests.Application.Queries
{
    public class GetCheckoutPriceTest
    {
        private Mock<IMoneyToCapitalService> _moneyToCapitalServiceMock;
        private Mock<IMediator> _mediator;
        private Guid _mainUserGuid;
        private IATASDbContext _dbContext;
        private Basket _basket;
        private Mock<IVatService> _vatService;
        private GetOrderPriceQueryHandler _handler;

        [SetUp]
        public async Task Decorate()
        {
            _moneyToCapitalServiceMock = new Mock<IMoneyToCapitalService>();
            _mediator = new Mock<IMediator>();
            _mainUserGuid = Guid.NewGuid();
            _dbContext = AtasContextFactory.Create(_mediator.Object);
            _vatService = new Mock<IVatService>();

            _basket = CustomerFactory.CreateBasket(_mainUserGuid);
            var seller = ProductsFactory.CreateMarket(_mainUserGuid);
            var subscriber = new Subscriber(_mainUserGuid);
            _dbContext.Baskets.Add(_basket);
            _dbContext.Sellers.Add(seller);
            _dbContext.Subscribers.Add(subscriber);

            _handler = new GetOrderPriceQueryHandler(
                _moneyToCapitalServiceMock.Object,
                _dbContext,
                _vatService.Object);

            await _dbContext.Save();
        }

        [Test]
        public async Task Get_Basket_Checkout_Price_WithoutPromoVatAndDiscount()
        {
            //Decorate
            var productPlan1 = await _dbContext.SubscriptionPlans.FindAsync(1);
            var productPlan2 = await _dbContext.SubscriptionPlans.FindAsync(2);

            _basket.AddBasketItem(productPlan1);
            _basket.AddBasketItem(productPlan2);


            await _dbContext.Save();

            var handler = new GetOrderPriceQueryHandler(
                _moneyToCapitalServiceMock.Object,
                _dbContext,
                _vatService.Object);

            //Act
            var result = await handler.Handle(new GetCheckoutPriceQuery()
            {
                Promocodes = null,
                CustomerId = _mainUserGuid,
                VatNumber = null
            }, CancellationToken.None);

            //Assetrs
            Assert.True(result.OrderItems.Count == 2);
            Assert.True(result.TotalPrice == 35);

            // Vat service must not called
            _vatService.Verify(x => x.GetVatRateByNumber(It.IsAny<string>()), Times.Never);


            //Money to text converter must called once
            _moneyToCapitalServiceMock.Verify(x => x.MoneyToCapital(It.IsAny<Money>()), Times.Once);
        }

        [Test]
        public async Task Get_Basket_CheckoutPrice_WithPromoPercent_WithoutVatAndDiscount()
        {
            //Decorate
            var promocode = PromocodeFactory.CreatePercentPromocode(1, 10);
            _dbContext.Promocodes.Add(promocode);
            _basket.AddBasketItem(await _dbContext.SubscriptionPlans.FindAsync(1)); // price 15
            _basket.AddBasketItem(await _dbContext.SubscriptionPlans.FindAsync(4)); // price 20
            await _dbContext.Save();

            //Act
            var result = await _handler.Handle(new GetCheckoutPriceQuery()
            {
                Promocodes = new[] {promocode.Code},
                CustomerId = _mainUserGuid,
                VatNumber = null
            }, CancellationToken.None);

            //Asserts
            // plan 1 = 15 -15/100*10 (13.5) + plan 2 = 20
            Assert.True(result.SubtotalPrice == 35);
            Assert.True(result.TotalPrice == 33.5m);
        }

        [Test]
        public async Task Get_Basket_CheckoutPrice_WithPromoAmount_WihoutVatAndDiscount()
        {
            //Decorate
            var promocode = PromocodeFactory.CreateAmountPromocode(1, 10);
            _dbContext.Promocodes.Add(promocode);
            _basket.AddBasketItem(await _dbContext.SubscriptionPlans.FindAsync(1)); // price 15
            _basket.AddBasketItem(await _dbContext.SubscriptionPlans.FindAsync(4)); // price 20


            await _dbContext.Save();


            //Act
            var result = await _handler.Handle(new GetCheckoutPriceQuery()
            {
                Promocodes = new[] {promocode.Code},
                CustomerId = _mainUserGuid,
                VatNumber = null
            }, CancellationToken.None);

            // plan 1 = 15 -10 + plan 2 = 20 
            // discount 10 (amount discount)
            Assert.True(result.TotalPrice == 25);
        }

        [Test]
        public async Task Get_Basket_CheckoutPrice_WithTwoSamePlanPromoAmount_WihoutVatAndDiscount()
        {
            //Promo for plan 4,6.
            //Discount $5.
            //Code "code64".
            //Finally prices $15 and $195 for plan 4 and 6 
            var promocode = PromocodeFactory.CreateAmountPromocode(new[] {6, 4}, 5);

            //Promo for plan 4,1.
            //Discount $10 (4 Must ignore).
            //Code "code41".
            //Finally $5 for plan 1 
            var promocode2 = PromocodeFactory.CreateAmountPromocode(new[] {4, 1}, 10);

            _dbContext.Promocodes.Add(promocode.promocode);
            _dbContext.Promocodes.Add(promocode2.promocode);

            _basket.AddBasketItem(await _dbContext.SubscriptionPlans.FindAsync(1)); // price 15
            _basket.AddBasketItem(await _dbContext.SubscriptionPlans.FindAsync(4)); // price 20
            _basket.AddBasketItem(await _dbContext.SubscriptionPlans.FindAsync(6)); // price 200

            await _dbContext.Save();

            //Act
            var result = await _handler.Handle(new GetCheckoutPriceQuery()
            {
                Promocodes = new[]
                {
                    promocode.code,
                    promocode2.code
                },
                CustomerId = _mainUserGuid,
                VatNumber = null
            }, CancellationToken.None);


            //Assert
            //Must 15 + 195 + 5 = 215 (against 235).
            //4 plan must be ignored at second promo
            Assert.AreEqual(result.TotalPrice, 215);
        }

        [Test]
        public async Task Get_Basket_CheckoutPrice_WithVat_WithoutPromoAndDiscount()
        {
            //Decorate
            const string vatNumber = "12345";

            _basket.AddBasketItem(await _dbContext.SubscriptionPlans.FindAsync(1)); // price 15

            _vatService.Setup(x => x.GetVatRateByNumber(vatNumber)).ReturnsAsync(new Vat(5));
            await _dbContext.Save();

            //Act
            var result = await _handler.Handle(new GetCheckoutPriceQuery()
            {
                Promocodes = null,
                CustomerId = _mainUserGuid,
                VatNumber = vatNumber
            }, CancellationToken.None);

            //Assert 
            Assert.AreEqual(result.TotalPrice, 15.75m);
            Assert.AreEqual(result.VatPrice, 0.75m);
        }

        // Пример расчета:
        // 01.01.2020 Юзер купил продукт MOEX, план 30 дней за 30 EUR (1 евро в день)
        // 10.01.2020 Юзеру дали бонус, добавили 10 дней к его подписке бесплатно.
        // 20.01.2020 Юзер покупает еще один план продукта MOEX на 90 дней за 70 EUR (0,78 евро в день)

        // Если 25.01.2020 юзер захочет купить план другого продукта, то остаточная стоимость будет считаться так:
        // У юзера на данный момент есть 105 дней до экспирации. Начинаем перебор его покупок с конца:
        // Учитываем последнюю покупку на 90 дней стоимостью 0.78 EUR/день. Отнимаем 90 дней от экспирации, остается 15 дней подписки. И размер скидки = 90*0.78=70 EUR
        // Учитываем бонус в 10 дней по нулевой цене. Остается 5 дней, размер скидки не меняется
        // Учитываем покупку от 1-го января. Остается 5 дней по цене 1 EUR/день. Итого общий размер скидки= 70 + 0 + 5= 75 EUR

        // Соответственно, даем юзеру купить что угодно, что дороже 75 EUR со скидкой 75 EUR


        [Test]
        public async Task Get_Basket_CheckoutPrice_WithUpgradeDiscount_WhitoutVatAndDiscount()
        {
            var owner = new Owner();

            var dateTimeMock = new Mock<IDateTime>();

            var subscriber = await _dbContext.Subscribers
                .Include(x => x.Subscriptions)
                .ThenInclude(x => x.SubscriptionPeriodExpansions)
                .FirstOrDefaultAsync(x => x.Id == _mainUserGuid);


            #region 01.01.2020 Юзер купил продукт MOEX, план 30 дней за 30 EUR (1 евро в день)

            dateTimeMock.SetupGet(x => x.Now).Returns(new DateTime(2020, 01, 01));

            var plan = await _dbContext
                .SubscriptionPlans
                .Include(x => x.Product)
                .FirstOrDefaultAsync(x => x.Id == 7);

            var orderItem = new OrderItem(plan.Id, plan.ProductId, "test", plan.Price);

            var order = CustomerFactory.CreateOrder(_mainUserGuid, orderItem);

            order.Payed("1234", "12345", _moneyToCapitalServiceMock.Object.MoneyToCapital, owner.BankRequsites,
                dateTimeMock.Object);

            _dbContext.Orders.Add(order);
            await _dbContext.Save();


            var subscription = new Subscription(
                orderItem.ProductPlanId,
                orderItem.ProductId,
                plan.Product.ProductGroupId,
                orderItem.Name,
                SubscriptionType.Live,
                plan.SubscriptionRoles.Select(x=>x.Id).ToList(),
                plan.CustomizationFields.OfType<CustomizationField>().ToList(),
                new SubscriptionPeriodExpansion(plan.DaysAllowedToUse, orderItem.Id, dateTimeMock.Object.Now));

            subscription.SubscriberId = _mainUserGuid;

            _dbContext.Subscriptions.Add(subscription);
            await _dbContext.Save();

            #endregion

            #region 10.01.2020 Юзеру дали бонус, добавили 10 дней к его подписке бесплатно.

            dateTimeMock.SetupGet(x => x.Now).Returns(new DateTime(2020, 01, 10));

            subscription.ExtendSubscriptionDays(new SubscriptionPeriodExpansion(10, null, dateTimeMock.Object.Now));

            await _dbContext.Save();

            #endregion

            #region 20.01.2020 Юзер покупает еще один план продукта MOEX на 90 дней за 70 EUR (0,78 евро в день)

            dateTimeMock.SetupGet(x => x.Now).Returns(new DateTime(2020, 01, 20));

            var planMoex90Days = await _dbContext
                .SubscriptionPlans
                .Include(x => x.Product)
                .FirstOrDefaultAsync(x => x.Id == 8);

            var orderItem2 = new OrderItem(planMoex90Days.Id, planMoex90Days.ProductId,
                "test", planMoex90Days.Price);

            var order2 = CustomerFactory.CreateOrder(_mainUserGuid, orderItem2);

            order2.Payed("1234", "12345", _moneyToCapitalServiceMock.Object.MoneyToCapital, owner.BankRequsites,
                dateTimeMock.Object);

            _dbContext.Orders.Add(order2);
            await _dbContext.Save();

            subscription.ExtendSubscriptionDays(new SubscriptionPeriodExpansion(planMoex90Days.DaysAllowedToUse,
                orderItem2.Id, dateTimeMock.Object.Now));

            await _dbContext.Save();

            #endregion

            dateTimeMock.SetupGet(x => x.Now).Returns(new DateTime(2020, 01, 25));

            var balance = subscription.GetBalanceInfo(dateTimeMock.Object);

            Assert.AreEqual(balance.BalanceAmount, 75);

            
            Assert.AreEqual((subscription.GetExpiration(true,true) - dateTimeMock.Object.Now).Days, 95);
            Assert.AreEqual((subscription.GetExpiration(true,false) - dateTimeMock.Object.Now).Days, 105);
            
            
        }
    }
}