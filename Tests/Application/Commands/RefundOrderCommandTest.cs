using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Interfaces.Services;
using Application.СQRS.Orders.Commands.RefundOrder;
using Common;
using Common.Enums;
using Common.ValueObjects;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.Enums;
using Domain.ValueObjects;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using Tests.Common;

namespace Tests.Application.Commands
{
    public class RefundOrderCommandTest
    {
        private readonly Guid _mainUserGuid = Guid.NewGuid();
        private RefundOrderCommandHandler _handler;
        private Mock<IPaymentService> _paymentServiceMock = new Mock<IPaymentService>();
        private IATASDbContext _dbContext = AtasContextFactory.Create(new Mock<IMediator>().Object);

        [SetUp]
        public void BeforeEach()
        {
            _handler = new RefundOrderCommandHandler(_dbContext, _paymentServiceMock.Object);
        }

        [Test]
        public async Task Try_Refund_Not_Payed_Order()
        {
            var dateTimeMock = new Mock<IDateTime>();
            dateTimeMock.SetupGet(x => x.Now).Returns(DateTime.Now);

            var order = new Order(_mainUserGuid, new List<OrderItem>()
            {
                new OrderItem(1, 1, "test", new Money(100, CurrencyType.Usd))
            }, new Vat(5), null, CustomerFactory.CreateRequisites(), PaymentType.Card, dateTimeMock.Object);


            _dbContext.Orders.Add(order);
            await _dbContext.Save();


            // Act assert

            Assert.ThrowsAsync<BadRequestException>(() => _handler.Handle(new RefundOrderCommand()
            {
                OrderId = order.Id,
            }, CancellationToken.None));
        }

        [Test]
        public async Task Refund_Not_Order()
        {
            var dateTimeMock = new Mock<IDateTime>();
            dateTimeMock.SetupGet(x => x.Now).Returns(DateTime.Now);

            var order = new Order(_mainUserGuid, new List<OrderItem>()
            {
                new OrderItem(1, 1, "test", new Money(100, CurrencyType.Usd))
            }, new Vat(5), null, CustomerFactory.CreateRequisites(), PaymentType.Card, dateTimeMock.Object);

            order.Payed("123", "123", null, OwnerFactory.Create().BankRequsites);

            _dbContext.Orders.Add(order);
            await _dbContext.Save();

            // Act

            await _handler.Handle(new RefundOrderCommand()
            {
                OrderId = order.Id,
            }, CancellationToken.None);

            order = await _dbContext.Orders.FirstOrDefaultAsync(x => x.Id == order.Id);


            // Asserts

            Assert.True(order.OrderStatus == OrderStatus.PaymentRefund);

            _paymentServiceMock.Verify(x => x.Refund(It.IsAny<Order>()), Times.Once);
        }
    }
}