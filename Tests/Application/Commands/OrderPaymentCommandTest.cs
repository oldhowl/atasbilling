using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.Common.Interfaces.Services;
using Application.СQRS.Orders.Commands.OrderPayment;
using Common;
using Common.Enums;
using Common.ValueObjects;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.Enums;
using Domain.ValueObjects;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using Tests.Common;

namespace Tests.Application.Commands
{
    public class OrderPaymentCommandTest
    {
        private Guid _mainUserGuid = Guid.NewGuid();
        private IATASDbContext _dbContext;
        private OrderPaymentHandler _orderPaymentHandler;
        private Mock<IDateTime> _dateTimeMock;

        [SetUp]
        public void BeforeEach()
        {
            var mediator = new Mock<IMediator>();
            _dbContext = AtasContextFactory.Create(mediator.Object);
            var invoiceSequence = new Mock<IInvoiceSequence>();
            var moneyToCapitalServiceMock = new Mock<IMoneyToCapitalService>();
            _dateTimeMock = new Mock<IDateTime>();
            _dateTimeMock.SetupGet(x => x.Now).Returns(DateTime.Now);
            invoiceSequence.Setup(x => x.GetNext()).Returns(Task.FromResult((100001.ToString(), 100005.ToString())));

            _orderPaymentHandler = new OrderPaymentHandler(_dbContext, invoiceSequence.Object,
                moneyToCapitalServiceMock.Object, _dateTimeMock.Object);
        }

        [Test]
        public async Task OrderPaymentTest()
        {
            var owner = OwnerFactory.Create();
             _dbContext.Owners.Add(owner);
            await _dbContext.Save();
            var order = new Order(_mainUserGuid, new List<OrderItem>()
            {
                new OrderItem(1, 1, "test", new Money(100, CurrencyType.Usd))
            }, new Vat(5), null, CustomerFactory.CreateRequisites(), PaymentType.Card, _dateTimeMock.Object);

            _dbContext.Orders.Add(order);
            await _dbContext.Save();
            
            //Act 
            
            await _orderPaymentHandler.Handle(new OrderPaymentCommand()
            {
                OrderId = order.Id
            }, CancellationToken.None);

            order = await _dbContext
                .Orders
                .Include(x => x.Invoice)
                .Include(x => x.ProformaInvoice)
                .FirstOrDefaultAsync(x => x.Id == order.Id);

            //Asserts
            
            Assert.True(order.PaymentInfo.IsPayed);
            Assert.NotNull(order.Invoice);
            Assert.NotNull(order.ProformaInvoice);

            Assert.AreEqual(order.Invoice.Number, 100001.ToString());
            Assert.AreEqual(order.ProformaInvoice.Number, 100005.ToString());
            Assert.True(order.PaymentInfo.TotalPrice == new Money(100, CurrencyType.Usd));
            Assert.True(order.Items.Count == 1);
            Assert.True(order.Items.First().ProductPlanId == 1);
            Assert.True(order.Items.First().ProductId == 1);
            Assert.True(order.Items.First().PositionName == "test");
            Assert.True(order.Vat.Rate == 5);
            Assert.True(order.Promocode is null);
            Assert.True(Equals(order.Invoice.Requsites, owner.BankRequsites));
            Assert.True(Equals(order.PaymentInfo.Requisites, CustomerFactory.CreateRequisites()));
        }
    }
}