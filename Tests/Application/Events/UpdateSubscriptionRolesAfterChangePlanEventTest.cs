using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.DomainEvents;
using Application.Common.Interfaces;
using Application.СQRS.Subscriptions.Events.SubscribeCustomerAfterPayment;
using Application.СQRS.Subscriptions.Events.UpdateSubscriptionRolesAfterChangePlan;
using Common;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.BoundedContexts.Selling.Events;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Domain.BoundedContexts.Subscriptions.Aggregate;
using Domain.BoundedContexts.Subscriptions.Entities;
using Domain.BoundedContexts.Subscriptions.Models;
using Domain.Enums;
using Domain.ValueObjects;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Moq;
using NUnit.Framework;
using Tests.Common;

namespace Tests.Application.Events
{
    public class UpdateSubscriptionRolesAfterChangePlanEventTest
    {
        private IATASDbContext _atasDbContext = AtasContextFactory.Create(new Mock<IMediator>().Object);
        private UpdateSubscriptionRolesAfterChangePlanEventHandler _handler;
        private Guid _customerId = Guid.NewGuid();
        private Mock<IDateTime> _dateTimeMock = new Mock<IDateTime>();

        public UpdateSubscriptionRolesAfterChangePlanEventTest()
        {
            _dateTimeMock.SetupGet(x => x.Now).Returns(new DateTime(2020, 01, 10));

            _handler = new UpdateSubscriptionRolesAfterChangePlanEventHandler(_atasDbContext);
        }

        [SetUp]
        public async Task BeforeEach()
        {
            var seller = ProductsFactory.CreateMarket(Guid.NewGuid());
            _atasDbContext.Sellers.Add(seller);
            await _atasDbContext.Save();

            var subscriber = new Subscriber(_customerId);
            _atasDbContext.Subscribers.Add(subscriber);
            await _atasDbContext.Save();
        }

        [Test]
        public async Task Update_Roles_In_Active_Subscriptions_Test()
        {
            var plan1 = await _atasDbContext.SubscriptionPlans.FirstOrDefaultAsync(x => x.Id == 1);
            var plan2 = await _atasDbContext.SubscriptionPlans.FirstOrDefaultAsync(x => x.Id == 10);


            List<OrderItem> orderItems = new List<OrderItem>()
            {
                new OrderItem(plan1.Id, plan1.ProductId, plan1.Title, plan1.Price),
                new OrderItem(plan2.Id, plan2.ProductId, plan2.Title, plan2.Price),
            };

            var order = new Order(_customerId, orderItems, new Vat(5), null, CustomerFactory.CreateRequisites(),
                PaymentType.Card, _dateTimeMock.Object);

            _atasDbContext.Orders.Add(order);
            await _atasDbContext.Save();

            var subscriber = await _atasDbContext
                .Subscribers
                .Include(x => x.Subscriptions)
                .ThenInclude(x => x.SubscriptionRoles)
                .ThenInclude(x => x.PlatformRole)
                .ThenInclude(x => x.Features)
                .FirstOrDefaultAsync(x => x.Id == _customerId);

            var subscribtionPlanDtos = new List<SubscriptionPlan>() {plan1, plan2}
                .Join(orderItems,
                    subs => subs.Id,
                    orderItem => orderItem.ProductPlanId,
                    (plan, item) => new SubscriptionPlanDto(
                        plan.Id,
                        item.Id,
                        plan.Name,
                        plan.DaysAllowedToUse,
                        plan.SubscriptionType,
                        plan.ProductId,
                        plan.Product.ProductGroupId,
                        plan.SubscriptionRoles.Select(x => x.Id).ToList(),
                        plan.CustomizationFields)
                ).ToList();

            subscriber.Subscribe(subscribtionPlanDtos, _dateTimeMock.Object.Now);

            await _atasDbContext.Save();

            var feature = new Feature("new title", "new test feauture", true);
            var role = new PlatformRole("new role", "TEST");
            role.AddFeatures(new List<Feature>() {feature});
            _atasDbContext.PlatformRoles.Add(role);
            await _atasDbContext.Save();


            await _handler.Handle(
                new DomainEventNotification<SubscriptionPlanRolesChangedEvent>(
                    new SubscriptionPlanRolesChangedEvent(plan1.Id, new List<int>() {role.Id}))
                , CancellationToken.None);
            
            Assert.True(
                subscriber
                    .Subscriptions
                    .SelectMany(x => x.SubscriptionRoles)
                    .Select(x => x.PlatformRole)
                    .SelectMany(x => x.Features)
                    .Any(x => x.Title == "new title"));
        }
    }
}