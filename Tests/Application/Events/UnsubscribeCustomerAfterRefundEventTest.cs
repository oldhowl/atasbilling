using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.DomainEvents;
using Application.Common.Interfaces;
using Application.СQRS.Subscriptions.Events.SubscribeCustomerAfterPayment;
using Application.СQRS.Subscriptions.Events.UnsubscribeCustomerAfterRefund;
using Common;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.BoundedContexts.Orders.Events;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Domain.BoundedContexts.Subscriptions.Aggregate;
using Domain.BoundedContexts.Subscriptions.Models;
using Domain.Enums;
using Domain.ValueObjects;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using Tests.Common;

namespace Tests.Application.Events
{
    public class UnsubscribeCustomerAfterRefundEventTest
    {
        private IATASDbContext _atasDbContext = AtasContextFactory.Create(new Mock<IMediator>().Object);
        private UnsubscribeCustomerAfterRefundEventHandler _handler;
        private Guid _customerId = Guid.NewGuid();
        private Mock<IDateTime> _dateTimeMock = new Mock<IDateTime>();

        public UnsubscribeCustomerAfterRefundEventTest()
        {
            _dateTimeMock.SetupGet(x => x.Now).Returns(new DateTime(2020, 01, 10));

            _handler = new UnsubscribeCustomerAfterRefundEventHandler(_atasDbContext);
        }

        [SetUp]
        public async Task BeforeEach()
        {
            var seller = ProductsFactory.CreateMarket(Guid.NewGuid());
            _atasDbContext.Sellers.Add(seller);
            await _atasDbContext.Save();

            var subscriber = new Subscriber(_customerId);
            _atasDbContext.Subscribers.Add(subscriber);
            await _atasDbContext.Save();
        }

        [Test]
        public async Task Unsubscribe_After_Refund()
        {
            var plan1 = await _atasDbContext.SubscriptionPlans.FirstOrDefaultAsync(x => x.Id == 1);
            var plan2 = await _atasDbContext.SubscriptionPlans.FirstOrDefaultAsync(x => x.Id == 10);

            //First purchase          
            List<OrderItem> orderItems = new List<OrderItem>()
            {
                new OrderItem(plan1.Id, plan1.ProductId, plan1.Title, plan1.Price),
                new OrderItem(plan2.Id, plan2.ProductId, plan2.Title, plan2.Price),
            };

            var order1 = new Order(_customerId, orderItems, new Vat(5), null, CustomerFactory.CreateRequisites(),
                PaymentType.Card, _dateTimeMock.Object);

            //Second purchase
            var plan3 = await _atasDbContext.SubscriptionPlans.FirstOrDefaultAsync(x => x.Id == 11);
            List<OrderItem> orderItems2 = new List<OrderItem>()
            {
                new OrderItem(plan3.Id, plan3.ProductId, plan3.Title, plan3.Price),
            };
            var order2 = new Order(_customerId, orderItems2, new Vat(5), null, CustomerFactory.CreateRequisites(),
                PaymentType.Card, _dateTimeMock.Object);


            _atasDbContext.Orders.Add(order1);
            _atasDbContext.Orders.Add(order2);
            await _atasDbContext.Save();

            var allOrderItems = new List<OrderItem>(orderItems);
            allOrderItems.AddRange(orderItems2);
            
            var subscriber = await _atasDbContext.Subscribers.Include(x => x.Subscriptions)
                .ThenInclude(x => x.SubscriptionPeriodExpansions).FirstOrDefaultAsync(x => x.Id == _customerId);

            var subscribtionPlanDtos = new List<SubscriptionPlan>() {plan1, plan2, plan3}
                .Join(allOrderItems,
                    subs => subs.Id,
                    orderItem => orderItem.ProductPlanId,
                    (plan, item) => new SubscriptionPlanDto(
                        plan.Id,
                        item.Id,
                        plan.Name,
                        plan.DaysAllowedToUse,
                        plan.SubscriptionType,
                        plan.ProductId,
                        plan.Product.ProductGroupId,
                        plan.SubscriptionRoles.Select(x=>x.Id).ToList(),
                        plan.CustomizationFields)
                ).ToList();
            
            subscriber.Subscribe(subscribtionPlanDtos, DateTimeOffset.Now);

            await _atasDbContext.Save();

            
            //Act
            
            await _handler.Handle(
                new DomainEventNotification<OrderRefundedEvent>(new OrderRefundedEvent(_customerId,
                    orderItems.Select(x => x.Id).ToArray())), CancellationToken.None);


            Assert.AreEqual(subscriber.ActiveSubscriptions.Count, 1);
        }
    }
}