using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.DomainEvents;
using Application.Common.Interfaces;
using Application.СQRS.Subscriptions.Events.SubscribeCustomerAfterPayment;
using Common;
using Common.Enums;
using Common.ValueObjects;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.BoundedContexts.Orders.Events;
using Domain.BoundedContexts.Subscriptions.Aggregate;
using Domain.Enums;
using Domain.ValueObjects;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Moq;
using NUnit.Framework;
using Tests.Common;

namespace Tests.Application.Events
{
    public class SubscribeCustomerAfterPaymentEventTest
    {
        private IATASDbContext _atasDbContext = AtasContextFactory.Create(new Mock<IMediator>().Object);
        private SubscribeCustomerAfterPaymentEventHandler _handler;
        private Guid _customerId = Guid.NewGuid();
        private Mock<IDateTime> _dateTimeMock = new Mock<IDateTime>();

        public SubscribeCustomerAfterPaymentEventTest()
        {
            _dateTimeMock.SetupGet(x => x.Now).Returns(new DateTime(2020, 01, 10));

            _handler = new SubscribeCustomerAfterPaymentEventHandler(_atasDbContext);
        }

        [SetUp]
        public async Task BeforeEach()
        {
            var seller = ProductsFactory.CreateMarket(Guid.NewGuid());
            _atasDbContext.Sellers.Add(seller);
            await _atasDbContext.Save();
            
            var subscriber = new Subscriber(_customerId);
            _atasDbContext.Subscribers.Add(subscriber);
            await _atasDbContext.Save();
        }

        [Test]
        public async Task Subscribe_Subscriber()
        {
            var plan1 = await _atasDbContext.SubscriptionPlans.FirstOrDefaultAsync(x => x.Id == 1);
            var plan2 = await _atasDbContext.SubscriptionPlans.FirstOrDefaultAsync(x => x.Id == 10);

          
            List<OrderItem> orderItems = new List<OrderItem>()
            {
                new OrderItem(plan1.Id, plan1.ProductId, plan1.Title, plan1.Price),
                new OrderItem(plan2.Id, plan2.ProductId, plan2.Title, plan2.Price),
            };
            
            var order = new Order(_customerId, orderItems, new Vat(5), null, CustomerFactory.CreateRequisites(), PaymentType.Card, _dateTimeMock.Object);

            _atasDbContext.Orders.Add(order);
            await _atasDbContext.Save();

            await _handler.Handle(
                new DomainEventNotification<OrderPayedEvent>(new OrderPayedEvent(_dateTimeMock.Object.Now, _customerId,
                    orderItems)), CancellationToken.None);

            var subscriber = await _atasDbContext.Subscribers.Include(x => x.Subscriptions)
                .ThenInclude(x => x.SubscriptionPeriodExpansions).FirstOrDefaultAsync(x => x.Id == _customerId);

            Assert.AreEqual(subscriber.Subscriptions.Count, 2);
            
            Assert.True(
                subscriber
                    .Subscriptions
                    .SelectMany(x => x.SubscriptionRoles)
                    .Select(x => x.PlatformRole)
                    .SelectMany(x => x.Features)
                    .Any(x => x.Title == "feature1"));
        }
    }
}