using Domain.BoundedContexts.Owner.Aggregate;

namespace Tests.Common
{
    public class OwnerFactory
    {
        public static Owner Create()
        {
            return new Owner();
        }
    }
}