using System;
using System.Collections.Generic;
using System.Linq;
using Common.Enums;
using Common.ValueObjects;
using Domain.BoundedContexts.Selling.Abstractions;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Domain.BoundedContexts.Subscriptions.Entities;
using Domain.Enums;
using Domain.ValueObjects;
using NUnit.Framework;

namespace Tests.Common
{
    public class ProductsFactory
    {
        public static Seller CreateMarket(Guid guid)
        {
            var seller = new Seller(guid);


            var feature1 = new Feature(1, "feature1", "test", true);
            var feature2 = new Feature(2, "feature2", "test", true);
            var feature3 = new Feature(3, "feature3", "test", true);
            var feature4 = new Feature(4, "feature4", "test", true);
            var feature5 = new Feature(5, "feature5", "test", true);
            var feature6 = new Feature(6, "feature6", "test", true);

            var productRole1 = new PlatformRole(1, "role1", "ROLE 1");
            productRole1.AddFeatures(new List<Feature>() {feature1});

            var productRole2 = new PlatformRole(2, "role2", "ROLE 2");
            productRole2.AddFeatures(new List<Feature>() {feature2, feature3});

            var productRole3 = new PlatformRole(3, "role3", "ROLE 3");
            productRole3.AddFeatures(new List<Feature>() {feature4, feature5, feature6});


            var subsPlan1 = new SubscriptionPlan(
                "subscription plan 1",
                new Money(15, CurrencyType.Usd),
                new Money(20, CurrencyType.Usd),
                1,
                false,
                true,
                false,
                "1",
                null,
                null,
                15,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole1})
            {
                Id = 1
            };

            var subsPlan2 = new SubscriptionPlan(
                "subscription plan 2",
                new Money(20, CurrencyType.Usd),
                new Money(25, CurrencyType.Usd),
                2,
                false,
                true,
                false,
                "2",
                null,
                null,
                30,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole2, productRole3})
            {
                Id = 2
            };

            var product = new SubscriptionProduct("subscription product 1", 2, true, guid)
            {
                Id = 1
            };
            product.AddPlan(subsPlan1);
            product.AddPlan(subsPlan2);

            var productGroup = new ProductGroup(guid, "productGroup")
            {
                Id = 1
            };

            var productGroup2 = new ProductGroup(guid, "productGroup")
            {
                Id = 2
            };
            productGroup.AddProduct(product);


            var feature7 = new Feature(7, "feature7", "test", true);
            var feature8 = new Feature(8, "feature8", "test", true);
            var feature9 = new Feature(9, "feature9", "test", true);
            var feature10 = new Feature(10, "feature10", "test", true);
            var feature11 = new Feature(11, "feature11", "test", true);
            var feature12 = new Feature(12, "feature12", "test", true);

            var productRole4 = new PlatformRole(4, "role4", "ROLE 4");
            productRole4.AddFeatures(new List<Feature>() {feature7});

            var productRole5 = new PlatformRole(5, "role5", "ROLE 5");
            productRole5.AddFeatures(new List<Feature>() {feature8, feature9});

            var productRole6 = new PlatformRole(6, "role6", "ROLE 6");
            productRole6.AddFeatures(new List<Feature>() {feature10, feature11, feature12});


            var subsPlan3 = new SubscriptionPlan(
                "subscription plan 3",
                new Money(100, CurrencyType.Usd),
                new Money(200, CurrencyType.Usd),
                1,
                false,
                true,
                false,
                "1",
                null,
                null,
                15,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole4})
            {
                Id = 3
            };

            var subsPlan4 = new SubscriptionPlan(
                "subscription plan 4",
                new Money(20, CurrencyType.Usd),
                new Money(25, CurrencyType.Usd),
                2,
                false,
                true,
                false,
                "2",
                null,
                null,
                30,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole5, productRole6})
            {
                Id = 4
            };

            var product2 = new SubscriptionProduct("subscription product 2", 2, true, guid)
            {
                Id = 2
            };
            product2.AddPlan(subsPlan3);
            product2.AddPlan(subsPlan4);


            productGroup.AddProduct(product2);

            seller.AddProductGroup(productGroup);
            seller.AddProductGroup(productGroup2);

            var feature13 = new Feature(13, "feature13", "test", true);
            var feature14 = new Feature(14, "feature14", "test", true);
            var feature15 = new Feature(15, "feature15", "test", true);
            var feature16 = new Feature(16, "feature16", "test", true);
            var feature17 = new Feature(17, "feature17", "test", true);
            var feature18 = new Feature(18, "feature18", "test", true);

            var productRole7 = new PlatformRole(7, "role4", "ROLE 4");
            productRole7.AddFeatures(new List<Feature>() {feature13});

            var productRole8 = new PlatformRole(8, "role5", "ROLE 5");
            productRole8.AddFeatures(new List<Feature>() {feature14, feature15});

            var productRole9 = new PlatformRole(9, "role6", "ROLE 6");
            productRole9.AddFeatures(new List<Feature>() {feature16, feature17, feature18});


            var subsPlan5 = new SubscriptionPlan(
                "subscription plan 5",
                new Money(100, CurrencyType.Usd),
                new Money(200, CurrencyType.Usd),
                1,
                false,
                true,
                false,
                "1",
                null,
                null,
                15,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole7})
            {
                Id = 5
            };

            var subsPlan6 = new SubscriptionPlan(
                "subscription plan 6",
                new Money(200, CurrencyType.Usd),
                new Money(25, CurrencyType.Usd),
                2,
                false,
                true,
                false,
                "2",
                null,
                null,
                30,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole8, productRole9})
            {
                Id = 6
            };
            var subsPlan7 = new SubscriptionPlan(
                "MOEX",
                new Money(30, CurrencyType.Usd),
                new Money(25, CurrencyType.Usd),
                2,
                false,
                true,
                false,
                "2",
                null,
                new List<ProductCustomizationField>()
                {
                    new ProductCustomizationField()
                    {
                        Name = "MaxConnections",
                        Type = CustomFieldType.Int,
                        Value = 30
                    }
                },
                30,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole8, productRole9})
            {
                Id = 7
            };

            var subsPlan8 = new SubscriptionPlan(
                "MOEX",
                new Money(70, CurrencyType.Usd),
                new Money(25, CurrencyType.Usd),
                2,
                false,
                true,
                false,
                "2",
                null,
                new List<ProductCustomizationField>()
                {
                    new ProductCustomizationField()
                    {
                        Name = "MaxConnections",
                        Type = CustomFieldType.Int,
                        Value = 60
                    }
                },
                90,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole8, productRole9})
            {
                Id = 8
            };

            var product3 = new SubscriptionProduct("subscription product 3", 3, true, guid)
            {
                Id = 3
            };

            product3.AddPlan(subsPlan5);
            product3.AddPlan(subsPlan6);
            product3.AddPlan(subsPlan7);
            product3.AddPlan(subsPlan8);

            var subsPlan9 = new SubscriptionPlan(
                "CME",
                new Money(70, CurrencyType.Usd),
                new Money(25, CurrencyType.Usd),
                2,
                false,
                true,
                false,
                "2",
                null,
                new List<ProductCustomizationField>()
                {
                    new ProductCustomizationField()
                    {
                        Name = "MaxConnections",
                        Type = CustomFieldType.Int,
                        Value = 80
                    }
                },
                90,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole8, productRole9})
            {
                Id = 9
            };

            var subsPlan10 = new SubscriptionPlan(
                "CME",
                new Money(76, CurrencyType.Usd),
                new Money(25, CurrencyType.Usd),
                2,
                false,
                true,
                false,
                "2",
                null,
                new List<ProductCustomizationField>()
                {
                    new ProductCustomizationField()
                    {
                        Name = "MaxConnections",
                        Type = CustomFieldType.Int,
                        Value = 80
                    }
                },
                90,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole8, productRole9})
            {
                Id = 10
            };

            var product4 = new SubscriptionProduct("subscription product 3", 3, true, guid)
            {
                Id = 4
            };
            product4.AddPlan(subsPlan9);
            product4.AddPlan(subsPlan10);


            productGroup.AddProduct(product3);
            productGroup.AddProduct(product4);


            var subsPlan11 = new SubscriptionPlan(
                "MOEX",
                new Money(30, CurrencyType.Usd),
                new Money(25, CurrencyType.Usd),
                2,
                false,
                true,
                false,
                "2",
                null,
                new List<ProductCustomizationField>()
                {
                    new ProductCustomizationField()
                    {
                        Name = "MaxConnections",
                        Type = CustomFieldType.Int,
                        Value = 30
                    }
                },
                30,
                SubscriptionType.Live,
                new List<PlatformRole>() {productRole8, productRole9})
            {
                Id = 11
            };

            var product5 = new SubscriptionProduct("product 5", 1, true, seller.Id);
            product5.Id = 5;
            product5.AddPlan(subsPlan11);
            productGroup2.AddProduct(product5);


            return seller;
        }
    }
}