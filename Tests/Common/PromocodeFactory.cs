using System;
using System.Linq;
using Domain.BoundedContexts.Promotion.Aggregate;
using Domain.BoundedContexts.Promotion.Enums;

namespace Tests.Common
{
    public class PromocodeFactory
    {
        public static Promocode CreatePercentPromocode(int productPlanId, decimal discountAmount)
        {
            return new Promocode(
                $"code{productPlanId}",
                new[] {productPlanId}.ToList(),
                10,
                DiscountType.Percent,
                discountAmount,
                $"test for {productPlanId}",
                false,
                DateTimeOffset.Now,
                null);
        }

        public static Promocode CreateAmountPromocode(int productPlanId, decimal discountAmount)
        {
            return new Promocode(
                $"code{productPlanId}",
                new[] {productPlanId}.ToList(),
                10,
                DiscountType.Amount,
                discountAmount,
                $"test for {productPlanId}",
                false,
                DateTimeOffset.Now,
                null);
        }

        public static (Promocode promocode, string code) CreateAmountPromocode(int[] productPlanIds, decimal discountAmount)
        {
            var code =$"code{string.Join(string.Empty, productPlanIds)}";

            return (new Promocode(
                $"code{string.Join(string.Empty, productPlanIds)}",
                productPlanIds.ToList(),
                10,
                DiscountType.Amount,
                discountAmount,
                $"test for {productPlanIds}",
                false,
                DateTimeOffset.Now,
                null), code);
        }
    }
}