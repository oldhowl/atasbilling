using System;
using Application.Common.DomainEvents;
using Castle.Core.Logging;
using Domain.BoundedContexts.Customer.Aggregate;
using Domain.BoundedContexts.Orders.Aggregate;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using Moq;
using Presistance;

namespace Tests.Common
{
    public class AtasContextFactory
    {
        public static AtasDbContext Create(IMediator mediator)
        {
            var options = new DbContextOptionsBuilder<AtasDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .ConfigureWarnings(x => x.Ignore(InMemoryEventId.TransactionIgnoredWarning))
                .Options;

            var logger = new Mock<ILogger<MediatrDomainEventDispatcher>>();
            var domainEventDispatcher = new MediatrDomainEventDispatcher(mediator, logger.Object);

            var context = new AtasDbContext(options, domainEventDispatcher);

            context.Database.EnsureCreated();

            return context;
        }

        public static void Destroy(AtasDbContext context)
        {
            context.Database.EnsureDeleted();

            context.Dispose();
        }
        
        
    }
}