using System;
using System.Collections.Generic;
using Common.Enums;
using Common.ValueObjects;
using Domain.BoundedContexts.Baskets.Aggregate;
using Domain.BoundedContexts.Customer.Aggregate;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.Enums;
using Domain.ValueObjects;

namespace Tests.Common
{
    public class CustomerFactory
    {
        public static Customer CreateCustomer(Guid guid)
        {
            return new Customer(guid,
                new Person("test", "test", new DateTime(1989, 05, 12),
                    new Address("rr", "ee", "rr", "ee", "rr", "ss", 123456),
                    new Contacts("123", "123", 123, "123", new PhoneNumber(1, "123123")))
            );
        }

        public static Order CreateOrder(Guid mainUserGuid, params OrderItem[] orderItem)
        {
            return new Order(mainUserGuid, new List<OrderItem>(orderItem), new Vat(3), null,
                CustomerFactory.CreateRequisites(), PaymentType.Bank);
        }

        public static Requisites CreateRequisites()
        {
            return new Requisites(
                "ivan",
                "Ivanov",
                "ivanov@mail.ru",
                "79123456789",
                new DateTime(1980, 10, 10),
                650000,
                "Russian Federation",
                "Kemerovo",
                "Lenina 10-10",
                "PODOROJNIQUE",
                "1234567890",
                EntityType.Individual
            );
        }

        public static Basket CreateBasket(Guid guid)
        {
            return new Basket(guid);
        }
    }
}