using System;
using System.Linq;
using System.Threading.Tasks;
using Application.Common.DomainEvents;
using Domain.BoundedContexts.Baskets.Aggregate;
using Domain.BoundedContexts.Baskets.Events;
using Domain.BoundedContexts.Baskets.Exceptions;
using Domain.BoundedContexts.Selling.Abstractions;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Moq;
using NUnit.Framework;

namespace Tests.Domain.BasketContextTests
{
    public class AddBasketItemCommandTest
    {
        private Guid _mainUserGuid;

        [SetUp]
        public void BeforeEach()
        {
            _mainUserGuid = Guid.NewGuid();
        }

        [Test]
        public void Add_Basket_Item()
        {
            //Arrange
            var basket = new Mock<Basket>();
            var basketItem = new Mock<ProductPlan>();

            //Act
            basket.Object.AddBasketItem(basketItem.Object);

            //Assert
            Assert.True(
                basket.Object.DomainEvents.Any(x => x is BasketItemAddedEvent));
            Assert.True(basket.Object.Items.Any(x => x.Equals(basketItem.Object)));
            Assert.True(basket.Object.Items.Count == 1);
        }

        [Test]
        public void Add_Same_Basket_Item()
        {
            var basket = new Mock<Basket>();
            var productPlan1 = new Mock<ProductPlan>();

            //Act
            basket.Object.AddBasketItem(productPlan1.Object);

            //Assert
            Assert.Throws<BasketAlreadyExistItemException>(() => basket.Object.AddBasketItem(productPlan1.Object));
        }

        [Test]
        public void Remove_BasketItem()
        {
            var basket = new Mock<Basket>();
            var productPlan1 = new Mock<SubscriptionPlan>();
            productPlan1.SetupGet(x =>x.Id).Returns(1);

            var productPlan2 = new Mock<SubscriptionPlan>();
            productPlan2.SetupGet(x =>x.Id).Returns(2);
            basket.Object.AddBasketItem(productPlan1.Object);
            basket.Object.AddBasketItem(productPlan2.Object);


            //Act
            basket.Object.RemoveBasketItem(1);

            //Assert
            Assert.True(
                basket.Object.DomainEvents.Any(x => x is BasketItemRemovedEvent));
            Assert.True(basket.Object.Items.Any(x => x.Id == 2));
            Assert.True(basket.Object.Items.Count == 1);
        }

        [Test]
        public void Clear_Basket()
        {
            var basket = new Mock<Basket>();
            var productPlan1 = new Mock<ProductPlan>();
            productPlan1.SetupGet(x =>x.Id).Returns(1);

            var productPlan2 = new Mock<SubscriptionPlan>();
            productPlan2.SetupGet(x =>x.Id).Returns(2);
            basket.Object.AddBasketItem(productPlan1.Object);
            basket.Object.AddBasketItem(productPlan2.Object);

            //Act
            basket.Object.Clear();

            //Assert
            Assert.True(!basket.Object.Items.Any());
        }
    }
}