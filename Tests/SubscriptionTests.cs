using System;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

namespace Tests
{
    public class Tests
    {
        class SubsExtension
        {
            public int Days { get; set; }
            public DateTime Created { get; set; }
        }

        private ICollection<SubsExtension> _extensions = new List<SubsExtension>();
        private DateTime Now { get; set; } = new DateTime(2020, 05, 10, 15, 00, 00);

        [SetUp]
        public void Setup()
        {
            _extensions.Add(new SubsExtension() {Created = new DateTime(2020, 05, 01, 00, 00, 00), Days = 14});
            _extensions.Add(new SubsExtension() {Created = new DateTime(2020, 05, 14, 00, 00, 00), Days = 14});
        }

        [Test]
        public void Customer_Active_Custom_Roles_After_Subscription()
        {
            DateTime? expiration = null;
            foreach (var extension in _extensions.OrderBy(x => x.Created))
            {
                if (expiration == null)
                {
                    expiration = extension.Created.AddDays(extension.Days);
                }
                else
                {
                    expiration = expiration.Value.AddDays(extension.Days);
                }
              
            }
        }
    }
}