using System;
using System.Collections.Generic;
using System.Net;
using Microsoft.AspNetCore.Identity;

namespace Infrastructure.Identity
{
    public class User : IdentityUser<Guid>
    {
        public Guid Id { get; private set; }

        public int EntityId { get; set; }

        public IPAddress IpAddressAtRegistration { get; set; }

        public DateTimeOffset RegistrationDate { get; set; }
        
        // public NotificationSettings NotificationSettings { get; set; }
        // public ICollection<Notification> Notifications { get; set; }
        // public ICollection<CustomerAcceptedNotificationVersion> CustomerAcceptedNotificationVersions { get; set; }


    }
}