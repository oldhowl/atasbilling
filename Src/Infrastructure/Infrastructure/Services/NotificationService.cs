using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.Notifications.Models;

namespace Infrastructure.Services
{
    public class NotificationService : INotificationService
    {
        public Task SendApproveNotification(MessageDto messageDto)
        {
            return Task.CompletedTask;
        }
    }
}