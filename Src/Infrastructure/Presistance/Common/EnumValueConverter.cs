using System;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Presistance.Common
{
    public class EnumValueConverter
    {
        public static ValueConverter Convert<T>() where T : Enum
        {
            return new ValueConverter<T, string>(
                v => v.ToString(),
                v => (T) Enum.Parse(typeof(T), v));
        }
    }
}