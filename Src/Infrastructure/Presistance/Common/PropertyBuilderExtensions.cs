using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;

namespace Presistance.Common
{
    public static class PropertyBuilderExtensions
    {
        public static PropertyBuilder<T> ConvertToJson<T>(this PropertyBuilder<T> builder)
        {
            return builder.HasConversion(obj =>
                        JsonConvert.SerializeObject(obj, new Newtonsoft.Json.Converters.StringEnumConverter()),
                    json => JsonConvert.DeserializeObject<T>(json,
                        new Newtonsoft.Json.Converters.StringEnumConverter()))
                .HasColumnType("jsonb");
        }

    }
}