using Common.ValueObjects;
using Domain.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Presistance.Common
{
    public class PersonNavigationBuilder
    {
        public static void Build<T>(OwnedNavigationBuilder<T, Person> obj) where T : class
        {
            obj.OwnsOne(m => m.Contacts, ContactsNavigationBuilder);
            obj.Property(x => x.Birthdate).HasColumnName("birthdate");
            
        }

        private static void ContactsNavigationBuilder(OwnedNavigationBuilder<Person, Contacts> obj)
        {
            obj.OwnsOne(x => x.PhoneNumber);
        }

    }
}