using System;
using System.Linq.Expressions;
using Common.Enums;
using Common.ValueObjects;
using Domain.BoundedContexts.Orders.Aggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Presistance.Common
{
    public class MoneyNavigationBuilder
    {
        
        public static void Build<T>(OwnedNavigationBuilder<T, Money> obj) where T : class
        {
            obj.OwnsOne(m => m.Currency, CurrencyNavigationBuilder);
        }

        private static void CurrencyNavigationBuilder(OwnedNavigationBuilder<Money, Currency> nb)
        {
            nb.Property(x => x.CurrencyType)
                .HasConversion(EnumValueConverter.Convert<CurrencyType>());

            nb.Ignore(x => x.DisplayingSubType);
        }

    }
}