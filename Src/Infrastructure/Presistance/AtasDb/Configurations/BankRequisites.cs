using Common.ValueObjects;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Presistance.AtasDb.Configurations
{
    public class BankRequisitesConfiguration : IEntityTypeConfiguration<BankRequsites>
    {
        public void Configure(EntityTypeBuilder<BankRequsites> builder)
        {
            builder.HasNoKey();
        }
    }
}