using System;
using Domain.BoundedContexts.Selling.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Presistance.Common;

namespace Presistance.AtasDb.Configurations
{
    public class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.ProductGroup).WithMany(x => x.Products).HasForeignKey(x=>x.ProductGroupId);
            builder.HasMany(x => x.ProductTranslationDescriptions).WithOne();
            builder.Property(x => x.ProductCustomizationFieldsTemplate).ConvertToJson();

            builder.HasMany(x => x.Plans).WithOne(x => x.Product).HasForeignKey(x => x.ProductId);
            var propinfo = typeof(Product).GetProperty(nameof(Product.Plans));
            var nav = builder
                .Metadata
                .FindNavigation(propinfo);
            nav.SetPropertyAccessMode(PropertyAccessMode.Property);
        }
    }
}