using Domain.BoundedContexts.Baskets.Aggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Presistance.AtasDb.Configurations
{
    public class BasketConfiguration : IEntityTypeConfiguration<Basket>
    {
        public void Configure(EntityTypeBuilder<Basket> builder)
        {
            builder.ToTable("baskets");

            builder.HasKey(x => x.Id);

            builder.HasMany(x => x.Items).WithOne();

             var propinfo = typeof(Basket).GetProperty(nameof(Basket.Items));
                        var nav = builder
                            .Metadata
                            .FindNavigation(propinfo);
                        nav.SetPropertyAccessMode(PropertyAccessMode.Property);

            
        }
    }
}