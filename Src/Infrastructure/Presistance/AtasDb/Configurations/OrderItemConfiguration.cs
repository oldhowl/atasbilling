using System;
using Common.ValueObjects;
using Domain.BoundedContexts.Orders.Aggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Presistance.Common;

namespace Presistance.AtasDb.Configurations
{
    public class OrderItemConfiguration : IEntityTypeConfiguration<OrderItem>
    {
        public void Configure(EntityTypeBuilder<OrderItem> builder)
        {
            builder.HasKey(x => x.Id);
            builder.OwnsOne(x => x.Price, MoneyNavigationBuilder.Build);
            
        }
    }
}