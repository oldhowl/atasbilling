using System;
using Domain.BoundedContexts.Selling.Abstractions;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Presistance.Common;

namespace Presistance.AtasDb.Configurations
{
    public class SubscriptionPlanConfiguration : IEntityTypeConfiguration<ProductPlan>
    {
        public void Configure(EntityTypeBuilder<ProductPlan> builder)
        {
            builder.HasKey(x => x.Id);

            builder.HasOne(x => x.Product).WithMany(x => x.Plans).HasForeignKey(x => x.ProductId);

            builder.OwnsOne(x => x.Price, MoneyNavigationBuilder.Build);

            builder.OwnsOne(x => x.OldPrice, MoneyNavigationBuilder.Build);

            builder.HasMany(x => x.ProductLanguageDescriptions).WithOne();

            builder.HasOne(x => x.EmailMessageTemplate).WithOne().IsRequired(false);

            builder.Property(x => x.CustomizationFields).ConvertToJson();
            
        }
    }
}