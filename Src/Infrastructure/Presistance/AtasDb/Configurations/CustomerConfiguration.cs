using Domain.BoundedContexts.Customer.Aggregate;
using Domain.BoundedContexts.Orders.Aggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Presistance.Common;

namespace Presistance.AtasDb.Configurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> builder)
        {
            builder.HasKey(x => x.Id);
            builder.OwnsOne(x => x.Person, navigationBuilder =>
            {
                navigationBuilder.OwnsOne(x => x.Address);
                navigationBuilder.OwnsOne(x => x.Contacts,
                    ownedNavigationBuilder => { ownedNavigationBuilder.OwnsOne(x => x.PhoneNumber); });
            });
            builder.HasMany(x => x.Orders).WithOne(x => x.Customer).HasForeignKey(x => x.CustomerId);

            var propinfo = typeof(Customer).GetProperty(nameof(Customer.Orders));
            var nav = builder
                .Metadata
                .FindNavigation(propinfo);
            nav.SetPropertyAccessMode(PropertyAccessMode.Property);
        }
    }
}