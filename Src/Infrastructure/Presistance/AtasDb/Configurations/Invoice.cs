using System.Security.Policy;
using Domain.BoundedContexts.Orders.Aggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Presistance.Common;

namespace Presistance.AtasDb.Configurations
{
    public class InvoiceConfiguration : IEntityTypeConfiguration<Invoice>
    {
        public void Configure(EntityTypeBuilder<Invoice> builder)
        {
            builder.Property(x => x.Requsites).ConvertToJson();
            builder.Property(x => x.BillTo).ConvertToJson();
            builder.Property(x => x.InvoiceItems).ConvertToJson();
        }
    }
}