using System;
using Domain.BoundedContexts.Subscriptions.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Presistance.AtasDb.Configurations
{
    public class PlatformRoleConfiguration : IEntityTypeConfiguration<PlatformRole>
    {
        public void Configure(EntityTypeBuilder<PlatformRole> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasMany(x => x.Features).WithOne();
            
        }
    }
}