using System;
using Domain.BoundedContexts.Promotion.Aggregate;
using Domain.BoundedContexts.Promotion.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Presistance.Common;

namespace Presistance.AtasDb.Configurations
{
    public class PromocodeConfiguration : IEntityTypeConfiguration<Promocode>
    {
        public void Configure(EntityTypeBuilder<Promocode> builder)
        {
            builder.HasKey(x => x.Code);
            builder.Property(x => x.DiscountType).HasConversion(EnumValueConverter.Convert<DiscountType>());
            builder.Property(x => x.EndDate).IsRequired(false);
            
            builder.Property(x => x.ProductPlanIds).ConvertToJson();

        }
    }
}