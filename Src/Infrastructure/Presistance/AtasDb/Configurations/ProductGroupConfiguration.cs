using System;
using Domain.BoundedContexts.Selling.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Presistance.AtasDb.Configurations
{
    public class ProductGroupConfiguration : IEntityTypeConfiguration<ProductGroup>
    {
        public void Configure(EntityTypeBuilder<ProductGroup> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Seller).WithMany(x => x.ProductGroups).HasForeignKey(x => x.SellerId);
            builder.HasMany(x => x.Products).WithOne(x => x.ProductGroup).HasForeignKey(x => x.ProductGroupId);
            
            var propinfo = typeof(ProductGroup).GetProperty(nameof(ProductGroup.Products));
            var nav = builder
                .Metadata
                .FindNavigation(propinfo);
            nav.SetPropertyAccessMode(PropertyAccessMode.Property);
        }
    }
}