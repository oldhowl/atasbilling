using System;
using System.Net.NetworkInformation;
using Domain.BoundedContexts.Subscriptions.Aggregate;
using Domain.BoundedContexts.Subscriptions.Entities;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Presistance.Common;

namespace Presistance.AtasDb.Configurations
{
    public class SubscriptionConfiguration : IEntityTypeConfiguration<Subscription>
    {
        public void Configure(EntityTypeBuilder<Subscription> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.Subscriber).WithMany(x => x.Subscriptions).HasForeignKey(x => x.SubscriberId);
            builder.Property(x => x.SubscriptionType).HasConversion(EnumValueConverter.Convert<SubscriptionType>());
            builder.HasMany(x => x.SubscriptionPeriodExpansions).WithOne(x=>x.Subscription).HasForeignKey(x => x.SubscriptionId);

            builder.HasMany(x => x.SubscriptionRoles).WithOne(x => x.Subscription).HasForeignKey(x => x.SubscriptionId);
            var propinfo = typeof(Subscription).GetProperty(nameof(Subscription.SubscriptionRoles));
            var nav = builder
                .Metadata
                .FindNavigation(propinfo);
            nav.SetPropertyAccessMode(PropertyAccessMode.Property);

            

            builder.Property(x => x.Pausings).ConvertToJson();
            builder.Property(x => x.CustomizationFields).ConvertToJson();
        }
    }
}