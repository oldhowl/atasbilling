using System.ComponentModel;
using System.Security.Cryptography.Xml;
using System.Security.Policy;
using System.Text.Json.Serialization;
using Common.ValueObjects;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Newtonsoft.Json;
using Presistance.Common;

namespace Presistance.AtasDb.Configurations
{
    public class OrderConfiguration : IEntityTypeConfiguration<Order>
    {
        public void Configure(EntityTypeBuilder<Order> builder)
        {
            builder.HasKey(x => x.Id);
            builder
                .Metadata
                .FindNavigation(nameof(Order.Items))
                .SetPropertyAccessMode(PropertyAccessMode.Field);

            builder.HasMany(x => x.Items).WithOne();
            
            builder.HasOne(x => x.Invoice).WithOne().IsRequired(false);
            builder.HasOne(x => x.ProformaInvoice).WithOne().IsRequired(false);

            builder.OwnsOne(x => x.Vat);

            builder.OwnsOne(x => x.PaymentInfo, nb =>
            {
                nb.Property(x => x.Requisites).ConvertToJson();
                nb.Property(x => x.PaymentType).HasConversion(EnumValueConverter.Convert<PaymentType>());
                nb.Property(x => x.PayDate).IsRequired(false);
                nb.Property(x => x.RefundDate).IsRequired(false);
                nb.OwnsOne(x => x.TotalPrice, MoneyNavigationBuilder.Build);
            });
        }

        
    }
}