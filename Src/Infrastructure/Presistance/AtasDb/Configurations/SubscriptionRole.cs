using Domain.BoundedContexts.Subscriptions.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Presistance.AtasDb.Configurations
{
    public class SubscriptionRoleConfiguration : IEntityTypeConfiguration<SubscriptionRole>
    {
        public void Configure(EntityTypeBuilder<SubscriptionRole> builder)
        {
            builder.HasKey(x => new {x.SubscriptionId, x.PlatformRoleId});

            builder.HasOne(x => x.Subscription).WithMany(x => x.SubscriptionRoles).HasForeignKey(x => x.SubscriptionId);
            builder.HasOne(x => x.PlatformRole).WithMany(x => x.SubscriptionRoles).HasForeignKey(x => x.PlatformRoleId);
        }
    }
}