using Domain.BoundedContexts.Subscriptions.Aggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Presistance.AtasDb.Configurations
{
    public class SubscriptionPeriodExpansionsConfiguration : IEntityTypeConfiguration<SubscriptionPeriodExpansion>
    {
        public void Configure(EntityTypeBuilder<SubscriptionPeriodExpansion> builder)
        {
            builder.HasKey(x => x.Id);
            builder.HasOne(x => x.OrderItem).WithOne().IsRequired(false);
        }
    }
}