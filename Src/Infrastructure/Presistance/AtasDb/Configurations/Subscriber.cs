using Domain.BoundedContexts.Subscriptions.Aggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Presistance.AtasDb.Configurations
{
    public class SubscriberConfiguration : IEntityTypeConfiguration<Subscriber>
    {
        public void Configure(EntityTypeBuilder<Subscriber> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Ignore(x => x.ActiveSubscriptions);
            builder.HasMany(x => x.Subscriptions).WithOne(x => x.Subscriber).HasForeignKey(x => x.SubscriberId);
            
        }
    }
}