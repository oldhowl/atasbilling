using System;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Presistance.Common;

namespace Presistance.AtasDb.Configurations
{
    public class SellerConfiguration : IEntityTypeConfiguration<Seller>
    {
        public void Configure(EntityTypeBuilder<Seller> builder)
        {
            builder.HasKey(x => x.Id);
            builder.Property(x => x.SellerRequisits).ConvertToJson();
            builder.Property(x => x.Representative).ConvertToJson();

            builder.HasMany(x => x.ProductGroups).WithOne(x => x.Seller).HasForeignKey(x => x.SellerId);
            
            var propinfo = typeof(Seller).GetProperty(nameof(Seller.ProductGroups));
            var nav = builder
                .Metadata
                .FindNavigation(propinfo);
            nav.SetPropertyAccessMode(PropertyAccessMode.Property);
        }
    }
}