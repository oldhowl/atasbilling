using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Common.Abstractions;
using Domain.Abstractions;
using Domain.BoundedContexts.Baskets.Aggregate;
using Domain.BoundedContexts.Customer.Aggregate;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.BoundedContexts.Owner.Aggregate;
using Domain.BoundedContexts.Promotion.Aggregate;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Domain.BoundedContexts.Subscriptions.Aggregate;
using Domain.BoundedContexts.Subscriptions.Entities;
using Domain.EventDispatcher;
using Microsoft.EntityFrameworkCore;

namespace Presistance
{
    public class AtasDbContext : DbContext, IATASDbContext
    {
        public DbSet<Order> Orders { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<SubscriptionPlan> SubscriptionPlans { get; set; }
        public DbSet<Subscription> Subscriptions { get; set; }
        public DbSet<Owner> Owners { get; set; }
        public DbSet<Promocode> Promocodes { get; set; }
        public DbSet<Subscriber> Subscribers { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<PlatformRole> PlatformRoles { get; set; }

        private readonly IDomainEventDispatcher _dispatcher;


        public AtasDbContext(DbContextOptions<AtasDbContext> options,
            IDomainEventDispatcher dispatcher)
            : base(options)
        {
            _dispatcher = dispatcher;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            var t = typeof(AtasDbContext).Assembly;
            modelBuilder.ApplyConfigurationsFromAssembly(t);
        }

        public override int SaveChanges()
        {
            PreSaveChanges().GetAwaiter().GetResult();
            var res = base.SaveChanges();
            return res;
        }

        public override async Task<int> SaveChangesAsync(
            CancellationToken cancellationToken = default(CancellationToken))
        {
            await PreSaveChanges();
            var res = await base.SaveChangesAsync(cancellationToken);
            return res;
        }

        private async Task PreSaveChanges()
        {
            await DispatchDomainEvents();
        }

        private async Task DispatchDomainEvents()
        {
            var domainEventEntities = ChangeTracker.Entries<IDomainEntity>()
                .Select(po => po.Entity)
                .Where(po => po.DomainEvents.Any())
                .ToArray();

            foreach (var entity in domainEventEntities)
                while (entity.DomainEvents.TryTake(out var dev))
                    await _dispatcher.Dispatch(dev);
        }
        public Task<int> Save(CancellationToken cancellationToken = default)
        {
            return SaveChangesAsync(cancellationToken);
        }

        public Task BeginTransaction()
        {
            return Database.BeginTransactionAsync();
        }

        public void CommitTransaction()
        {
            Database.CommitTransaction();
        }
    }
}