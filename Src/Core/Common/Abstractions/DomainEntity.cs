using System.Collections.Concurrent;

namespace Common.Abstractions
{
        public abstract class DomainEntity : IDomainEntity
        {     
            private readonly ConcurrentQueue<IDomainEvent> _domainEvents = new ConcurrentQueue<IDomainEvent>();

            public IProducerConsumerCollection<IDomainEvent> DomainEvents => _domainEvents;

            protected void PublishEvent(IDomainEvent @event)
            {
                _domainEvents.Enqueue(@event);
            }

        }
}