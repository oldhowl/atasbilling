using System.Collections.Concurrent;

namespace Common.Abstractions
{
        public interface IDomainEntity
        {
            IProducerConsumerCollection<IDomainEvent> DomainEvents { get; }
        }
}