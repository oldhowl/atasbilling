using System;

namespace Common.Abstractions
{
    public class AuditableEvent : EventBase
    {
        public int Id { get; set; }
        public string ObjectId { get; set; }
        public string ObjectType { get; set; }
        public string ObjectName { get; set; }
        public string ActionDescription { get; protected set; }
        public int ActionForUserId { get; protected set; }

        
        public AuditableEvent(string objectId, string objectType, string objectName, string actionDescription = null)
        {
            ObjectId = objectId;
            ObjectType = objectType;
            ObjectName = objectName;
            Date = DateTime.Now;
            ActionDescription = actionDescription;
        }
        public int ActionByUserId { get; protected set; }
        public DateTime Date { get; protected set; }

        public void AddAuditableInformation(
            int actionByUserId,
            int actionForUserId)
        {
            
            ActionByUserId = actionByUserId;
            Date = DateTime.Now;
            ActionForUserId = actionForUserId;
        }
    }
}