using System.Collections.Generic;

namespace Common.ValueObjects
{
    public class BankRequsites : ValueObject
    {
        public string CompanyName { get; private set; } 
        public string LegalAddress { get; private set; } 
        public string RegistrationNumber { get; private set; }
        public string VatNmber { get; private set; } 
        public string BankName { get; private set; }
        public string BankAddress { get; private set; } 
        public string BankBicSwift { get; private set; } 
        public string AccountNumber { get; private set; }
        public string PayPalPaymentId { get; private set; }

        
        // private BankRequsites(){}
        public BankRequsites()
        {
            CompanyName = "ATAS Ltd.";
            LegalAddress = "1 Katlakalna street, Riga, Latvia, LV-1073";
            RegistrationNumber = "40203154738";
            VatNmber = "LV40203154738";
            BankName  = "Citadele banka";
            BankAddress= "2A Republikas laukums, Riga, Latvia";
            BankBicSwift = "PARXLV22XXX";
            AccountNumber = "LV46 PARX 0021 0694 1000 2";
            PayPalPaymentId = "";
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return CompanyName;
            yield return LegalAddress;
            yield return RegistrationNumber;
            yield return VatNmber;
            yield return BankName;
            yield return BankAddress;
            yield return BankBicSwift;
            yield return AccountNumber;
            yield return PayPalPaymentId;
        }
    }
}