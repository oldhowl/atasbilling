using System;
using System.Collections.Generic;
using Common.Enums;

namespace Common.ValueObjects
{
    public class Requisites : ValueObject
    {
        public string Name { get; private set; }
        public string Surname { get; private set; }
        public string Email { get; private set; }
        public string PhoneNumber { get; private set; }
        public DateTime BirthDate { get; private set; }
        public int ZipCode { get; private set; }
        public string Country { get; private set; }
        public string City { get; private set; }
        public string Address { get; private set; }
        public string CompanyName { get; private set; }
        public string RegistrationNumber { get; private set; }

        public EntityType EntityType { get; private set; }

        public Requisites(string name, string surname, string email, string phoneNumber, DateTime birthDate,
            int zipCode, string country, string city, string address, string companyName, string registrationNumber,
            EntityType entityType)
        {
            Name = name;
            Surname = surname;
            Email = email;
            PhoneNumber = phoneNumber;
            BirthDate = birthDate;
            ZipCode = zipCode;
            Country = country;
            City = city;
            Address = address;
            CompanyName = companyName;
            RegistrationNumber = registrationNumber;
            EntityType = entityType;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Name;
            yield return Surname;
            yield return Email;
            yield return PhoneNumber;
            yield return BirthDate;
            yield return ZipCode;
            yield return Country;
            yield return City;
            yield return Address;
            yield return CompanyName;
            yield return RegistrationNumber;
            yield return EntityType;
        }
    }
}