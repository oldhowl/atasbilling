using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization;
using Common.Enums;

namespace Common.ValueObjects
{
    public class Currency : IEquatable<Currency>
    {
        public string IsoCode { get;  }
        public bool IsDigital { get;  }
        public string GeneralName { get;  }
        public string Symbol { get;  }
        public int DecimalPlace { get;  }
        public int BaseDecimalPlace { get;  }
        public string DecimalMark { get;  }
        public string ThousandMark { get;  }
        public Dictionary<string, CurrencySubType> SubTypes;
        public CurrencySubType DisplayingSubType { get; private set; }
        
        private CurrencyTypeRepository _repo = new CurrencyTypeRepository();
        public CurrencyType CurrencyType { get; private set; }

        public Currency(CurrencyType currencyType) : this()
        {
            CurrencyType = currencyType;
        }

        private void BuildCurrency()
        {
          
        }

        public Currency()
        {
            if (!_repo.Exists(CurrencyType))
                throw new ArgumentException("Invalid ISO Currency Code");

            var newCurrency = _repo.Get(CurrencyType);
            IsoCode = newCurrency.IsoCode;
            IsDigital = newCurrency.IsDigital;
            GeneralName = newCurrency.GeneralName;
            Symbol = newCurrency.Symbol;
            DecimalPlace = newCurrency.DecimalPlace;
            BaseDecimalPlace = newCurrency.BaseDecimalPlace;
            DecimalMark = newCurrency.DecimalMark;
            ThousandMark = newCurrency.ThousandMark;
        }

        public Currency(string isoCode, bool isDigital, string generalName, string symbol, int decimalPlace,
            int baseDecimalPlace, string decimalMark, string thousandMark)
        {
            IsoCode = isoCode;
            IsDigital = isDigital;
            GeneralName = generalName;
            Symbol = symbol;
            DecimalPlace = decimalPlace;
            BaseDecimalPlace = baseDecimalPlace;
            DecimalMark = decimalMark;
            ThousandMark = thousandMark;
        }

        public Currency(string isoCode, bool isDigital, string generalName, string symbol, int decimalPlace,
            int baseDecimalPlace, string decimalMark, string thousandMark, Dictionary<string, CurrencySubType> subtypes)
        {
            IsoCode = isoCode;
            IsDigital = isDigital;
            GeneralName = generalName;
            Symbol = symbol;
            DecimalPlace = decimalPlace;
            BaseDecimalPlace = baseDecimalPlace;
            DecimalMark = decimalMark;
            ThousandMark = thousandMark;
            SubTypes = subtypes;
        }

        #region Equals and !Equals

        public bool Equals(Currency other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return IsoCode == other.IsoCode;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != typeof(Currency)) return false;
            return Equals((Currency) obj);
        }

        public override int GetHashCode()
        {
            return IsoCode.GetHashCode();
        }

        public static bool operator ==(Currency left, Currency right)
        {
            return Equals(left, right);
        }

        public static bool operator !=(Currency left, Currency right)
        {
            return !Equals(left, right);
        }

        #endregion

        public CurrencySubType GetSubType(string key)
        {
            if (SubTypes != null)
            {
                if (SubTypes.ContainsKey(key))
                    return SubTypes[key];
            }

            return null;
        }

        public void DisplayAsSubType(string key)
        {
            DisplayingSubType = GetSubType(key);
        }

        public string GetStringFormat()
        {
            string decimalZero = "";
            for (int i = 1; i <= this.DecimalPlace; i++)
            {
                decimalZero += "0";
            }

            string specifier = "#" + this.ThousandMark + "0" + this.DecimalMark + decimalZero + ";(#,0." + decimalZero +
                               ")";
            return specifier;
        }

        public override string ToString()
        {
            return this.IsoCode + Symbol;
        }

        /// <summary>
        /// Display any passed in Money object decorated by its own Currency object
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public string ToString(Money m)
        {
            string displaySymbol = m.Currency.Symbol;
            decimal displayAmount = m.Amount;

            if (m.Currency.DisplayingSubType != null)
            {
                displaySymbol = m.Currency.DisplayingSubType.Symbol;
                displayAmount = m.Currency.DisplayingSubType.ScaleToUnit * m.Amount;
            }

            return displaySymbol + displayAmount.ToString(GetStringFormat());
        }
    }


    public class CurrencySubType
    {
        public string Symbol { get; set; }
        public int ScaleToUnit { get; set; }
    }


    public class CurrencyTypeRepository
    {
        // List of all currencies with their properties.
        public static readonly Dictionary<CurrencyType, Currency> Currencies =
            new Dictionary<CurrencyType, Currency>()
            {
                {CurrencyType.Eur, new Currency("EUR", false, "Euro", "€", 2, 2, ".", ",")},
                {CurrencyType.Rub, new Currency("RUB", false, "Russian ruble", "PP", 2, 2, ".", ",")},
                {CurrencyType.Usd, new Currency("USD", false, "US dollar", "$", 2, 2, ".", ",")},
            };

        public Currency Get(CurrencyType currencyType)
        {
            if (Currencies.ContainsKey(currencyType))
            {
                return Currencies[currencyType];
            }
            else
            {
                return null;
            }
        }

        public bool Exists(CurrencyType currencyType)
        {
            return Currencies.ContainsKey(currencyType);
        }
    }
}