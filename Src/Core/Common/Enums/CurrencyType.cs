namespace Common.Enums
{
    public enum CurrencyType
    {
        Eur,
        Rub,
        Usd
    }
}