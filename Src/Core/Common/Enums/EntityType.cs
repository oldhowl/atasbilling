namespace Common.Enums
{
    public enum EntityType
    {
        Individual,
        Legal
    }
}