using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Logging.CriticalEvents;
using Application.Extensions;
using Domain.BoundedContexts.Subscriptions.Aggregate;
using Domain.BoundedContexts.Subscriptions.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Subscriptions.Commands.ReleaseSubscription
{
    public class ReleaseSubscriptionCommand : INotification
    {
        public ReleaseSubscriptionCommand(int subscriptionId)
        {
            SubscriptionId = subscriptionId;
        }

        public int SubscriptionId { get; }
    }

    public class ReleaseSubscriptionCommandHandler : INotificationHandler<ReleaseSubscriptionCommand>
    {
        private readonly IATASDbContext _dbContext;
        private readonly ILoggerBag _loggerBag;


        public ReleaseSubscriptionCommandHandler(IATASDbContext dbContext, ILoggerBag loggerBag)
        {
            _dbContext = dbContext;
            _loggerBag = loggerBag;
        }

        public async Task Handle(ReleaseSubscriptionCommand notification, CancellationToken cancellationToken)
        {
            var subscription = await _dbContext
                .Subscriptions
                .Include(x => x.SubscriptionPeriodExpansions)
                .FirstOrDefaultAsync(x => x.Id == notification.SubscriptionId);

            if (subscription is null) throw new NotFoundException(nameof(Subscription), notification.SubscriptionId);
            
            var previousVersion = subscription.Clone();

            try
            {
                subscription.Release();
                _loggerBag.AddEvent(new SubscriptionModifiedEvent(subscription.Id, previousVersion, subscription));
            }
            catch (SubscriptionException e)
            {
                throw new BadRequestException(e.Message);
            }
        }
    }
}