using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Logging.CriticalEvents;
using Application.Extensions;
using Domain.BoundedContexts.Subscriptions.Aggregate;
using Domain.BoundedContexts.Subscriptions.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Subscriptions.Commands.PauseSubscription
{
    public class PauseSubscriptionCommand : INotification
    {
        public PauseSubscriptionCommand(int subscriptionId)
        {
            SubscriptionId = subscriptionId;
        }

        public int SubscriptionId { get; }
    }

    public class PauseSubscriptionCommandHandler : INotificationHandler<PauseSubscriptionCommand>
    {
        private readonly IATASDbContext _dbContext;
        private readonly ILoggerBag _loggerBag;


        public PauseSubscriptionCommandHandler(IATASDbContext dbContext, ILoggerBag loggerBag)
        {
            _dbContext = dbContext;
            _loggerBag = loggerBag;
        }

        public async Task Handle(PauseSubscriptionCommand notification, CancellationToken cancellationToken)
        {
            var subscription = await _dbContext
                .Subscriptions
                .Include(x => x.SubscriptionPeriodExpansions)
                .FirstOrDefaultAsync(x => x.Id == notification.SubscriptionId);

            if (subscription is null) throw new NotFoundException(nameof(Subscription), notification.SubscriptionId);

            var previousVersion = subscription.Clone();
            try
            {
                subscription.Pause();
                _loggerBag.AddEvent(new SubscriptionModifiedEvent(subscription.Id, previousVersion, subscription));
            }
            catch (SubscriptionException e)
            {
                throw new BadRequestException(e.Message);
            }
        }
    }
}