using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.DomainEvents;
using Application.Common.Interfaces;
using Domain.BoundedContexts.Selling.Events;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Subscriptions.Events.UpdateSubscriptionRolesAfterChangePlan
{
    public class
        UpdateSubscriptionRolesAfterChangePlanEventHandler : INotificationHandler<
            DomainEventNotification<SubscriptionPlanRolesChangedEvent>>
    {
        private readonly IATASDbContext _dbContext;

        public UpdateSubscriptionRolesAfterChangePlanEventHandler(IATASDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Handle(DomainEventNotification<SubscriptionPlanRolesChangedEvent> notification,
            CancellationToken cancellationToken)
        {
            var domainEventArgs = notification.DomainEventArgs;

            var subscriptions =
                await _dbContext.Subscriptions
                    .Include(x => x.SubscriptionRoles)
                    .Where(x => domainEventArgs.PlanProductRoleIds.Contains(x.ProductPlanId))
                    .ToListAsync();

            subscriptions.ForEach(x => x.UpdateRoles(domainEventArgs.PlanProductRoleIds));

            await _dbContext.Save(cancellationToken);
        }
    }
}