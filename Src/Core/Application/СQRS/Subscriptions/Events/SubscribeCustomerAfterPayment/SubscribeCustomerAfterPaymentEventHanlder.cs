using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.DomainEvents;
using Application.Common.Interfaces;
using Domain.BoundedContexts.Orders.Events;
using Domain.BoundedContexts.Subscriptions.Models;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Subscriptions.Events.SubscribeCustomerAfterPayment
{
    public class
        SubscribeCustomerAfterPaymentEventHandler : INotificationHandler<DomainEventNotification<OrderPayedEvent>>
    {
        private IATASDbContext _dbContext;

        public SubscribeCustomerAfterPaymentEventHandler(IATASDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Handle(DomainEventNotification<OrderPayedEvent> notification,
            CancellationToken cancellationToken)
        {
            // var (subscriber, plans) = await FetchData(notification);

            var domainEventArgs = notification.DomainEventArgs;

            var subscriber = await _dbContext
                .Subscribers
                .Include(x => x.Subscriptions)
                .FirstOrDefaultAsync(
                    subs => subs.Id == domainEventArgs.CustomerId, cancellationToken: cancellationToken);

            var plans = await _dbContext.SubscriptionPlans
                .Include(x => x.Product)
                .Where(plan =>
                    domainEventArgs.SubscriptionOrderItems.Select(x => x.ProductPlanId).Contains(plan.Id)
                ).ToListAsync();

            var subscribtionPlanDtos = plans
                .Join(domainEventArgs.SubscriptionOrderItems,
                    subs => subs.Id,
                    orderItem => orderItem.ProductPlanId,
                    (plan, item) => new SubscriptionPlanDto(
                        plan.Id,
                        item.Id,
                        plan.Name,
                        plan.DaysAllowedToUse,
                        plan.SubscriptionType,
                        plan.ProductId,
                        plan.Product.ProductGroupId,
                        plan.SubscriptionRoles.Select(x=>x.Id).ToList(),
                        plan.CustomizationFields)
                ).ToList();

            subscriber.Subscribe(subscribtionPlanDtos, domainEventArgs.PayedDate);

            await _dbContext.Save();
        }
    }
}