using System.Threading;
using System.Threading.Tasks;
using Application.Common.DomainEvents;
using Application.Common.Interfaces;
using Domain.BoundedContexts.Orders.Events;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Subscriptions.Events.UnsubscribeCustomerAfterRefund
{
    public class
        UnsubscribeCustomerAfterRefundEventHandler : INotificationHandler<DomainEventNotification<OrderRefundedEvent>>
    {
        private IATASDbContext _atasDbContext;

        public UnsubscribeCustomerAfterRefundEventHandler(IATASDbContext atasDbContext)
        {
            _atasDbContext = atasDbContext;
        }

        public async Task Handle(DomainEventNotification<OrderRefundedEvent> notification,
            CancellationToken cancellationToken)
        {
            var domainEventArgs = notification.DomainEventArgs;

            var subscriber = await
                _atasDbContext.Subscribers
                    .Include(x => x.Subscriptions)
                    .ThenInclude(x => x.SubscriptionPeriodExpansions)
                    .FirstOrDefaultAsync(x => x.Id == domainEventArgs.CustomerId);

            subscriber.ReduceSubscriptionsAfterRefund(domainEventArgs.SubscriptionOrderItemIds);

            await _atasDbContext.Save();
        }
    }
}