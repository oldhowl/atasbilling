using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Interfaces.Services;
using Domain.BoundedContexts.Orders.Exceptions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Orders.Commands.RefundOrder
{
    public class RefundOrderCommand : INotification
    {
        public int OrderId { get; set; }
    }

    public class RefundOrderCommandHandler : INotificationHandler<RefundOrderCommand>
    {
        private readonly IATASDbContext _atasDbContext;
        private readonly IPaymentService _paymentService;

        public RefundOrderCommandHandler(IATASDbContext atasDbContext, IPaymentService paymentService)
        {
            _atasDbContext = atasDbContext;
            _paymentService = paymentService;
        }

        public async Task Handle(RefundOrderCommand notification, CancellationToken cancellationToken)
        {
            var order = await _atasDbContext.Orders
                .Include(x => x.Items)
                .FirstOrDefaultAsync(x => x.Id == notification.OrderId, cancellationToken: cancellationToken);

            await _atasDbContext.BeginTransaction();

            try
            {
                order.Refund();
            }
            catch (OrderCannotBeRefundedException e)
            {
                throw new BadRequestException(e.Message);
            }

            await _paymentService.Refund(order);

            await _atasDbContext.Save(cancellationToken);

            _atasDbContext.CommitTransaction();
        }
    }
}