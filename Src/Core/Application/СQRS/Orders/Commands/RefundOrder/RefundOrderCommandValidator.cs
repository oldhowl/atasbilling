using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Domain.BoundedContexts.Orders.Specifications;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Orders.Commands.RefundOrder
{
    public class RefundOrderCommandValidator : AbstractValidator<RefundOrderCommand>
    {
        private readonly IATASDbContext _dbContext;

        public RefundOrderCommandValidator(IATASDbContext dbContext)
        {
            _dbContext = dbContext;
            RuleFor(x => x.OrderId)
                .MustAsync(LastOrderToRefund);
        }

        private async Task<bool> LastOrderToRefund(int orderId, CancellationToken cancellationToken)
        {
            var activeOrders = await _dbContext.Orders.Where(
                    OrderThatCanBeRefundedSpecification.Build(orderId)
                ).OrderByDescending(x => x.PaymentInfo.PaymentType)
                .Select(x => x.Id)
                .ToListAsync();

            return activeOrders.Count switch
            {
                //Orders is empty
                0 => false, 
                
                //Single order of condition
                1 => true, 
                
                //Order need to refund is first in queue
                _ => (activeOrders.IndexOf(0) == orderId)  
            };
        }
    }
}