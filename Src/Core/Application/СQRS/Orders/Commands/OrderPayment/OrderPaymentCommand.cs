using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.Common.Interfaces.Services;
using Common;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Orders.Commands.OrderPayment
{
    public class OrderPaymentCommand : INotification
    {
        public int OrderId { get; set; }
    }

    public class OrderPaymentHandler : INotificationHandler<OrderPaymentCommand>
    {
        private readonly IATASDbContext _dbContext;
        private readonly IInvoiceSequence _invoiceSequence;
        private readonly IMoneyToCapitalService _moneyToCapitalService;
        private readonly IDateTime _dateTime;

        public OrderPaymentHandler(
            IATASDbContext dbContext,
            IInvoiceSequence invoiceSequence,
            IMoneyToCapitalService moneyToCapitalService,
            IDateTime dateTime)
        {
            _dbContext = dbContext;
            _invoiceSequence = invoiceSequence;
            _moneyToCapitalService = moneyToCapitalService;
            _dateTime = dateTime;
        }

        public async Task Handle(OrderPaymentCommand notification, CancellationToken cancellationToken)
        {
            var orderTask = _dbContext
                .Orders
                .Include(x => x.Items)
                .FirstOrDefaultAsync(x => x.Id == notification.OrderId, cancellationToken);

            var seqTask = _invoiceSequence.GetNext();

            var ownerTask = _dbContext.Owners.FirstOrDefaultAsync(cancellationToken: cancellationToken);

            await Task.WhenAll(orderTask, seqTask, ownerTask);

            var (invoiceNum, proformaInvoiceNum) = await seqTask;
            var order = await orderTask;
            
            order.Payed(invoiceNum, proformaInvoiceNum, _moneyToCapitalService.MoneyToCapital,
                ownerTask.Result.BankRequsites, _dateTime);

            await _dbContext.Save(cancellationToken);
        }
    }
}