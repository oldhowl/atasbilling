using System;
using System.Linq;
using Application.СQRS.Baskets.Queries.GetCheckoutPrice;
using Common.ValueObjects;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.BoundedContexts.Owner.Aggregate;
using Domain.Enums;
using Domain.ValueObjects;

namespace Application.СQRS.Orders.Queries.OrderCheckout
{
    public class BasketToOrderMapper
    {
        public static Order Map(
            Owner owner,
            Guid customerId,
            decimal vat,
            OrderPriceFinalPriceDto orderPriceFinalPriceDto,
            RequisitesDto requisitesDto,
            PaymentType paymentType)
        {
            var orderItems = orderPriceFinalPriceDto.OrderItems.Select(orderItem =>
                new OrderItem(
                    orderItem.PlanId,
                    orderItem.ProductId,
                    orderItem.Title,
                    new Money(orderItem.Price, orderItem.CurrencyType))
            ).ToList();

            var requisites = new Requisites(
                requisitesDto.Name,
                requisitesDto.Surname,
                requisitesDto.Email,
                requisitesDto.PhoneNumber,
                requisitesDto.BirthDate,
                requisitesDto.ZipCode,
                requisitesDto.Country,
                requisitesDto.City,
                requisitesDto.Address,
                requisitesDto.CompanyName,
                requisitesDto.RegistrationNumber,
                requisitesDto.EntityType
            );


            return new Order(
                customerId,
                orderItems,
                new Vat(vat),
                string.Join(", ", orderPriceFinalPriceDto.Promocodes.Select(x => x.Code)),
                requisites,
                paymentType
            );
        }
    }
}