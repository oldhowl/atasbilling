namespace Application.СQRS.Orders.Queries.OrderCheckout
{
    public enum CheckoutResultAction
    {
        RedirectToInvoicePage,
        RedirectToPaymentGateway
    }
}