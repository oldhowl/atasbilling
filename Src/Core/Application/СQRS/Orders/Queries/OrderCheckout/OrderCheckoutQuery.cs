using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.Common.Interfaces.Services;
using Application.СQRS.Baskets.Queries.GetCheckoutPrice;
using Domain.BoundedContexts.Baskets.Aggregate;
using Domain.Enums;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Orders.Queries.OrderCheckout
{
    public class OrderCheckoutQuery : IRequest<OrderCheckoutResult>
    {
        public PaymentType PaymentType { get; set; }
        public Guid CustomerId { get; set; }
        public RequisitesDto Requisites { get; set; }
        public Guid BasketId { get; set; }
        public string VatNumber { get; set; }
    }


    public class OrderCheckoutHandler : IRequestHandler<OrderCheckoutQuery, OrderCheckoutResult>
    {
        private readonly IATASDbContext _atasDbContext;
        private readonly IMediator _mediator;
        private readonly IPaymentService _paymentService;

        public OrderCheckoutHandler(
            IATASDbContext atasDbContext,
            IMediator mediator, IPaymentService paymentService)
        {
            _atasDbContext = atasDbContext;
            _mediator = mediator;
            _paymentService = paymentService;
        }

        //TODO: Refactoring to SQL
        public async Task<OrderCheckoutResult> Handle(OrderCheckoutQuery notification,
            CancellationToken cancellationToken)
        {
            var checkoutOrderPriceTask = _mediator
                .Send(
                    new GetCheckoutPriceQuery
                    {
                        CustomerId = notification.CustomerId,
                        VatNumber = notification.VatNumber
                    }, cancellationToken);

            var productOwnerTask = _atasDbContext.Owners.FirstOrDefaultAsync(cancellationToken: cancellationToken);

            await Task.WhenAll(checkoutOrderPriceTask, productOwnerTask);

            var checkoutOrderPrice = await checkoutOrderPriceTask;
            var productOwner = await productOwnerTask;

            await _atasDbContext.BeginTransaction();

            _atasDbContext.Baskets.Remove(new Basket {Id = notification.BasketId});

            var order = BasketToOrderMapper.Map(
                productOwner,
                notification.CustomerId,
                checkoutOrderPrice.VatPrice,
                checkoutOrderPrice,
                notification.Requisites,
                notification.PaymentType);

            _atasDbContext.Orders.Add(order);
            await _atasDbContext.Save(cancellationToken);

            var checkoutOrderResult = notification.PaymentType == PaymentType.Bank
                ? new OrderCheckoutResult(null, CheckoutResultAction.RedirectToInvoicePage)
                : new OrderCheckoutResult(await _paymentService.GetPaymentUrl(order, notification.PaymentType),
                    CheckoutResultAction.RedirectToPaymentGateway);

            _atasDbContext.CommitTransaction();

            return checkoutOrderResult;
        }
    }
}