using MediatR;

namespace Application.СQRS.Orders.Queries.OrderCheckout
{
    public class OrderCheckoutResult : IRequest
    {
        public string PaymentReidrectUrl { get; }
        public CheckoutResultAction CheckoutResultAction { get; }

        public OrderCheckoutResult(string paymentReidrectUrl, CheckoutResultAction checkoutResultAction)
        {
            PaymentReidrectUrl = paymentReidrectUrl;
            CheckoutResultAction = checkoutResultAction;
        }
    }
}