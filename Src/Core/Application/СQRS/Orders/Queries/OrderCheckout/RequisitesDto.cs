using System;
using Common.Enums;

namespace Application.СQRS.Orders.Queries.OrderCheckout
{
    public class RequisitesDto 
    {
        public string Name { get;  set; }
        public string Surname { get;  set; }
        public string Email { get;  set; }
        public string PhoneNumber { get;  set; }
        public DateTime BirthDate { get;  set; }
        public int ZipCode { get;  set; }
        public string Country { get;  set; }
        public string City { get;  set; }
        public string Address { get;  set; }
        public string CompanyName { get;  set; }
        public string RegistrationNumber { get;  set; }

        public EntityType EntityType { get;  set; }
    }
}