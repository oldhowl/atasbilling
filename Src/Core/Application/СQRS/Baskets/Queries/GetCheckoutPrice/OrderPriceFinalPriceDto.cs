using System.Collections.Generic;
using System.Linq;

namespace Application.СQRS.Baskets.Queries.GetCheckoutPrice
{
    public class OrderPriceFinalPriceDto
    {
        public IList<OrderItemDto> OrderItems { get; set; } = new List<OrderItemDto>();
        public IList<PromocodeDto> Promocodes { get; set; } = new List<PromocodeDto>();

        public decimal TotalPrice => OrderItems.Sum(x => x.FinalPrice) + VatPrice;
        public decimal SubtotalPrice => OrderItems.Sum(x => x.Price);

        public decimal VatPrice { get; set; }

        public string TotalPriceText { get; set; }
    }
}