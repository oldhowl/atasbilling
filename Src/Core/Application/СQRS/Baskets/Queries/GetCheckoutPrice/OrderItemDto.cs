using Common.Enums;

namespace Application.СQRS.Baskets.Queries.GetCheckoutPrice
{
    public class OrderItemDto
    {
        public int PlanId { get; set; }
        public int ProductId { get; set; }
        public int ProductGroupId { get; set; }
        public int SubscriptionDays { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public decimal FinalPrice => Price - DiscountPrice;
        public CurrencyType CurrencyType { get; set; }
        public string CurrencySymbol { get; set; }
        
        
        public decimal DiscountPrice { get; set; }
        public bool IsDiscounted { get; set; }


        public OrderItemDto(
            int planId,
            int productId,
            int productGroupId,
            int subscriptionDays,
            string title,
            decimal price,
            string currencySymbol)
        {
            SubscriptionDays = subscriptionDays;
            ProductId = productId;
            ProductGroupId = productGroupId;
            PlanId = planId;
            CurrencySymbol = currencySymbol;
            Title = title;
            Price = price;
        }

        public void Discount(decimal discountAmount)
        {
            IsDiscounted = true;
            DiscountPrice = discountAmount;
        }
    }
}