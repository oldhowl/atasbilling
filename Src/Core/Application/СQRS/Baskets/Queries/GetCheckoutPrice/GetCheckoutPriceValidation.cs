using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Domain.BoundedContexts.Promotion.Specifications;
using FluentValidation;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace Application.СQRS.Baskets.Queries.GetCheckoutPrice
{
    public class GetCheckoutPriceValidation : AbstractValidator<GetCheckoutPriceQuery>
    {
        private readonly IATASDbContext _dbContext;

        public GetCheckoutPriceValidation(IATASDbContext dbContext)
        {
            _dbContext = dbContext;

            RuleFor(x => x.CustomerId).MustAsync(BasketIsNotEmpty);

            RuleForEach(x => x.Promocodes).MustAsync(PromocodeActive);
        }

        private Task<bool> PromocodeActive(string promocode, CancellationToken cancellationToken)
        {
            return _dbContext.Promocodes.AnyAsync(ActivePromocodeSpecification.Build(promocode));
        }

        private Task<bool> BasketIsNotEmpty(Guid customerId, CancellationToken cancellationToken)
        {
            return _dbContext.Baskets.AnyAsync(x => x.Id == customerId && x.Items.Count > 0,
                cancellationToken: cancellationToken);
        }
        
       
    }
}