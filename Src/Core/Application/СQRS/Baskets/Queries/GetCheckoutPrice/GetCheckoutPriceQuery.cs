using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.PortableExecutable;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.Common.Interfaces.Services;
using Application.Extensions;
using Common.Enums;
using Common.ValueObjects;
using Domain.BoundedContexts.Baskets.Aggregate;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.BoundedContexts.Orders.Specifications;
using Domain.BoundedContexts.Promotion.Aggregate;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Domain.BoundedContexts.Subscriptions.Aggregate;
using Domain.Enums;
using Domain.ValueObjects;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

namespace Application.СQRS.Baskets.Queries.GetCheckoutPrice
{
    /// <summary>
    /// In there we build basket view model with final prices
    /// conserding active subscribes orders to discount  
    /// </summary>
    public class GetCheckoutPriceQuery : IRequest<OrderPriceFinalPriceDto>
    {
        public Guid CustomerId { get; set; }
        public string VatNumber { get; set; }

        private string[] _promocodes;

        public string[] Promocodes
        {
            get { return _promocodes?.Select(x => x.ToUpper()).ToArray(); }
            set { _promocodes = value; }
        }
    }

    public class GetOrderPriceQueryHandler : IRequestHandler<GetCheckoutPriceQuery, OrderPriceFinalPriceDto>
    {
        private readonly IMoneyToCapitalService _moneyToCapitalService;
        private readonly IATASDbContext _dbContext;
        private readonly IVatService _vatService;

        public GetOrderPriceQueryHandler(
            IMoneyToCapitalService moneyToCapitalService,
            IATASDbContext dbContext,
            IVatService vatService)
        {
            _moneyToCapitalService = moneyToCapitalService;
            _dbContext = dbContext;
            _vatService = vatService;
        }

        public async Task<OrderPriceFinalPriceDto> Handle(GetCheckoutPriceQuery request,
            CancellationToken cancellationToken)
        {
            var (subscriber, vat, promocodes, basket) = await FetchData(request, cancellationToken);

            var checkoutTotalPriceVm = new OrderPriceFinalPriceDto();


            if (!basket.Items.Any())
                return checkoutTotalPriceVm;

            CurrencyType? basketCurrency = null;

            foreach (var basketOrderItem in basket.Items)
            {
                var plan = basket.Items.OfType<SubscriptionPlan>().FirstOrDefault(x => x.Id == basketOrderItem.Id);

                if (plan is null)
                    continue;

                if (basketCurrency == null)
                    basketCurrency = plan.Price.Currency.CurrencyType;

                Money basketItemPrice = plan.Price;


                //Apply discount for plan upgrade if it possible
                // if (orderItems.Count > 0)
                //     if (_orderService.IsPlanNeedToUpgrade(plan.ProductId, plan.Product.ProductGroupId, orderItems))
                //         basketItemPrice -= _orderService.GetActiveSubscriptionBalance(
                //             basketOrderItem.ProductId,
                //             basketOrderItem.ProductId,
                //             orderItems);

                var upgradeInfo =
                    subscriber.GetDiscountUpgradeBalance(plan.Id, plan.ProductId, plan.Product.ProductGroupId);
                if (upgradeInfo != null)
                {
                    basketItemPrice -= upgradeInfo.BalanceAmount;
                }

                var orderItemDto = new OrderItemDto
                (plan.Id,
                    plan.ProductId,
                    plan.Product.ProductGroupId,
                    plan.DaysAllowedToUse,
                    plan.Name,
                    basketItemPrice.Amount,
                    basketItemPrice.Currency.Symbol
                );

                checkoutTotalPriceVm.OrderItems.Add(orderItemDto);
            }

            ApplyPromoDiscount(promocodes, checkoutTotalPriceVm);

            //Apply VAT
            checkoutTotalPriceVm.VatPrice = CalculateVatPrice(vat, checkoutTotalPriceVm.TotalPrice);

            checkoutTotalPriceVm.TotalPriceText =
                _moneyToCapitalService.MoneyToCapital(new Money(checkoutTotalPriceVm.TotalPrice, basketCurrency.Value));

            return checkoutTotalPriceVm;
        }

        private static void ApplyPromoDiscount(List<Promocode> promocodes,
            OrderPriceFinalPriceDto checkoutTotalPriceFinalPriceDto)
        {
            foreach (var promocode in promocodes)
            {
                var promoIsActive = false;

                var orderItems =
                    checkoutTotalPriceFinalPriceDto.OrderItems
                        .Where(x =>
                            !x.IsDiscounted
                            &&
                            promocode.ProductPlanIds.Contains(x.PlanId));
                foreach (var orderItem in orderItems)
                {
                    promoIsActive = true;
                    orderItem.Discount(promocode.CalculateDiscountPrice(orderItem.Price));
                }


                checkoutTotalPriceFinalPriceDto.Promocodes.Add(new PromocodeDto()
                {
                    Code = promocode.Code,
                    IsActive = promoIsActive
                });
            }
        }

        private async Task<(
            Subscriber subscriber,
            Vat vat,
            List<Promocode> promocodes,
            Basket Basket)> FetchData(GetCheckoutPriceQuery request,
            CancellationToken cancellationToken)
        {
            
            var vatTask = GetVat(request.VatNumber);


            var promocodesTask = _dbContext.Promocodes
                .Where(x => request.Promocodes.Contains(x.Code))
                .AsNoTracking()
                .ToListAsync();

            var basketTask = _dbContext.Baskets
                .Include(x => x.Items)
                .ThenInclude(x => x.Product)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == request.CustomerId, cancellationToken);

            var subscriberTask = _dbContext.Subscribers
                .Include(x => x.Subscriptions)
                .ThenInclude(x => x.SubscriptionPeriodExpansions)
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.Id == request.CustomerId, cancellationToken);

            await Task.WhenAll(subscriberTask, vatTask, promocodesTask, basketTask);

            var promocodes = await promocodesTask;

            //Sorting promocodes according to the entered data in request;
            if (promocodesTask.Result.Count > 0)
                promocodes = promocodes.SortBy(request.Promocodes, x => x.Code).ToList();


            return (
                await subscriberTask,
                await vatTask,
                promocodes,
                await basketTask);
        }

        private static decimal CalculateVatPrice(Vat vat, decimal price)
        {
            return vat?.CalculateVat(price) ?? 0;
        }


        private async Task<Vat> GetVat(string vatNumber)
        {
            return !string.IsNullOrEmpty(vatNumber)
                ? await _vatService.GetVatRateByNumber(vatNumber)
                : default;
        }

        private async Task<List<OrderItem>> GetActiveOrderItems(Guid customerId,
            CancellationToken cancellationToken)
        {
            return await _dbContext
                .Orders
                .Where(OrderPayedSpecification.ByCustomerId(customerId))
                .SelectMany(x => x.Items)
                .ToListAsync(cancellationToken);
        }
    }
}