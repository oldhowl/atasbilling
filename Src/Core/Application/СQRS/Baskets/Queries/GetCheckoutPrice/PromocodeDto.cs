namespace Application.СQRS.Baskets.Queries.GetCheckoutPrice
{
    public class PromocodeDto
    {
        public string Code { get; set; }
        public bool IsActive { get; set; }
    }
}