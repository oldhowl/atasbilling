using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Baskets.Commands.RemoveBasketItem
{
    public class RemoveBasketItemCommand : INotification
    {
        public Guid CustomerId { get; set; }
        public int BasketItemId { get; set; }
    }
    
    public class RemoveBasketItemCommandHandler : INotificationHandler<RemoveBasketItemCommand>
    {
        private readonly IATASDbContext _atasDbContext;

        public RemoveBasketItemCommandHandler(IATASDbContext atasDbContext)
        {
            _atasDbContext = atasDbContext;
        }

        public async Task Handle(RemoveBasketItemCommand notification, CancellationToken cancellationToken)
        {
            var basket = await _atasDbContext.Baskets
                .Include(x => x.Items)
                .FirstOrDefaultAsync(x => x.Id == notification.CustomerId);
            
            basket.RemoveBasketItem(notification.BasketItemId);

            await _atasDbContext.Save();
        }
    }
}