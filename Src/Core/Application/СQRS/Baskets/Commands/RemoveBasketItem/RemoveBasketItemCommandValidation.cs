using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Baskets.Commands.RemoveBasketItem
{
    public class RemoveBasketItemCommandValidation : AbstractValidator<RemoveBasketItemCommand>
    {
        private IATASDbContext _atasDbContext;

        public RemoveBasketItemCommandValidation(IATASDbContext atasDbContext)
        {
            _atasDbContext = atasDbContext;

            RuleFor(x => x).MustAsync(BasketItemOwnedByCustomer);
        }

        private Task<bool> BasketItemOwnedByCustomer(RemoveBasketItemCommand command,
            CancellationToken cancellationToken)
        {
            return _atasDbContext.Baskets.AnyAsync(x =>
                x.Id == command.CustomerId && x.Items.Any(item => item.Id == command.BasketItemId));
        }
    }
}