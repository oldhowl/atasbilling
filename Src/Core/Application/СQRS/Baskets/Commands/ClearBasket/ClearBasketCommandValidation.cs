using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using FluentValidation;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Baskets.Commands.ClearBasket
{
    public class ClearBasketCommandValidation : AbstractValidator<ClearBasketCommand>
    {
        private readonly IATASDbContext _dbContext;

        public ClearBasketCommandValidation(IATASDbContext dbContext)
        {
            _dbContext = dbContext;
            
            RuleFor(x => x.CustomerId).MustAsync(CustomerBasketExist);

            RuleFor(addOrderItemCommand => addOrderItemCommand.CustomerId).MustAsync(CustomerExist);
        }

        private Task<bool> CustomerBasketExist(Guid customerId, CancellationToken cancellationToken)
        {
            return _dbContext.Baskets.AnyAsync(x => x.Id == customerId);
        }

        private async Task<bool> CustomerExist(Guid customerId, CancellationToken cancellationToken)
        {
            return await _dbContext.Customers.AnyAsync(x => x.Id == customerId, cancellationToken: cancellationToken);
        }
    }
}