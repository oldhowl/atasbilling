using System;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Baskets.Commands.ClearBasket
{
    public class ClearBasketCommand : INotification
    {
        public Guid CustomerId { get; set; }
    }

    public class ClearBasketCommandHandler : INotificationHandler<ClearBasketCommand>
    {
        private readonly IATASDbContext _dbContext;

        public ClearBasketCommandHandler(IATASDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Handle(ClearBasketCommand notification, CancellationToken cancellationToken)
        {
            var basket = await _dbContext.Baskets
                .Include(x => x.Items)
                .FirstOrDefaultAsync(x => x.Id == notification.CustomerId);
                

            basket.Clear();

            await _dbContext.Save();
        }
    }
}