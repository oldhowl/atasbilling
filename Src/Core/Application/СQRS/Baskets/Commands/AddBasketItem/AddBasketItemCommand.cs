using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Common;
using Domain.BoundedContexts.Baskets.Aggregate;
using Domain.BoundedContexts.Baskets.Exceptions;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.BoundedContexts.Selling.Abstractions;
using MediatR;
using Microsoft.EntityFrameworkCore;

namespace Application.СQRS.Baskets.Commands.AddBasketItem
{
    public class AddBasketItemCommand : IRequest
    {
        public int ProductPlanId { get; set; }
        public Guid CustomerId { get; set; }
    }

    public class AddBasketCommandItemHandler : IRequestHandler<AddBasketItemCommand>
    {
        private IATASDbContext _dbContext;
        private readonly IDateTime _dateTime;

        public AddBasketCommandItemHandler(IATASDbContext dbContext, IDateTime dateTime)
        {
            _dbContext = dbContext;
            _dateTime = dateTime;
        }

        public async Task<Unit> Handle(AddBasketItemCommand notification, CancellationToken cancellationToken)
        {
            var productPlan = await _dbContext.SubscriptionPlans.FindAsync(notification.ProductPlanId);

            if (productPlan is null)
                throw new NotFoundException(nameof(ProductPlan), notification.ProductPlanId);

            var basket = await _dbContext.Baskets.FirstOrDefaultAsync(
                x => x.Id == notification.CustomerId,
                cancellationToken);

            if (!await ProductCanBeAdded(notification.ProductPlanId, notification.CustomerId, cancellationToken,
                _dateTime))
                throw new BadRequestException("Product can't be added");

            try
            {
                basket.AddBasketItem(productPlan);
            }
            catch (BasketException e)
            {
                throw new BadRequestException(e.Message);
            }

            await _dbContext.Save(cancellationToken);

            return Unit.Value;
        }

        private async Task<bool> ProductCanBeAdded(int productPlanId,
            Guid customerId,
            CancellationToken cancellationToken, IDateTime dateTime = null)
        {
            var subscriberTask = _dbContext
                .Subscribers
                .Include(x => x.Subscriptions)
                .ThenInclude(x => x.SubscriptionPeriodExpansions)
                .ThenInclude(x => x.OrderItem)
                .FirstOrDefaultAsync(x => x.Id == customerId);

            var productPlanTask =
                _dbContext.SubscriptionPlans
                    .FirstOrDefaultAsync(
                        subscriptionPlan => subscriptionPlan.Id == productPlanId,
                        cancellationToken: cancellationToken);


            await Task.WhenAll(subscriberTask, productPlanTask);
            var subscriber = await subscriberTask;
            var plan = await productPlanTask;

            var upgradeInfo =
                subscriber.GetDiscountUpgradeBalance(plan.Id, plan.ProductId, plan.Product.ProductGroupId);
            if (upgradeInfo != null)
                return upgradeInfo.BalanceAmount > plan.Price.Amount;

            return true;
        }
    }
}