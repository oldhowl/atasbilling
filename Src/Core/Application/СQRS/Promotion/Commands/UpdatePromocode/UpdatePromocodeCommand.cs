using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Interfaces;
using Application.Extensions;
using Domain.BoundedContexts.Promotion.Enums;
using MediatR;

namespace Application.СQRS.Promotion.Commands.UpdatePromocode
{
    public class UpdatePromocodeCommand : INotification
    {
        public string Code { get; set; }

        public int MaxUseCount { get; set; }
        public DiscountType DiscountType { get; set; }
        public decimal DiscountAmount { get; set; }
        public int[] ProductPlanIds { get; set; }
        public DateTimeOffset? EndDate { get; set; }

        public bool IsActive { get; set; }
        public string Description { get; set; }

        public bool IsPriority { get; set; }

        public DateTimeOffset StartDate { get; set; }
    }

    public class UpdatePromocodeCommandHandler : INotificationHandler<UpdatePromocodeCommand>
    {
        private readonly IATASDbContext _dbContext;

        public UpdatePromocodeCommandHandler(IATASDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task Handle(UpdatePromocodeCommand notification, CancellationToken cancellationToken)
        {
            await _dbContext.BeginTransaction();
            var promocode = await _dbContext.Promocodes.FindAsync(notification.Code);
            
            var previousVersion = promocode.Clone();

            promocode.MaxUseCount = notification.MaxUseCount;
            promocode.Description = notification.Description;
            promocode.IsPriority = notification.IsPriority;
            promocode.StartDate = notification.StartDate;


            if (promocode.IsActive != notification.IsActive
                || promocode.DiscountType != notification.DiscountType
                || promocode.DiscountAmount != notification.DiscountAmount
                || promocode.EndDate != notification.EndDate
                || !promocode.ProductPlanIds.Except(notification.ProductPlanIds).Any()
                && !notification.ProductPlanIds.Except(promocode.ProductPlanIds).Any())

                // promocode.Events.Enqueue(new PromocodeCriticalChangesEvent(notification.Code,
                // notification.DiscountType == DiscountType.Percent, notification.DiscountAmount,
                // notification.ProductPlanIds, notification.EndDate));


            promocode.DiscountType = notification.DiscountType;
            promocode.DiscountAmount = notification.DiscountAmount;
            promocode.UpdateProductPlans(notification.ProductPlanIds);
            promocode.EndDate = notification.EndDate;

            // promocode.Events
            //     .Enqueue(new PromocodeModifiedEvent(previousVersion, promocode));
            //
            // promocode.Events.Enqueue(
            //     new PromocodeChangedEvent(promocode.Code,
            //     promocode.DiscountType == DiscountType.Percent, promocode.DiscountAmount,
            //     promocode.ProductPlanIds.ToArray(), promocode.EndDate));

            

            await _dbContext.Save();

            _dbContext.CommitTransaction();
        }
    }
}