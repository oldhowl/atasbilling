using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Application.Common.Exceptions;
using Application.Common.Interfaces;
using Application.Common.Logging.CriticalEvents;
using Application.Extensions;
using Common.Enums;
using Common.ValueObjects;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using MediatR;
using Microsoft.EntityFrameworkCore;
using ProductPlan = Domain.BoundedContexts.Selling.Abstractions.ProductPlan;

namespace Application.СQRS.Products.Commands.UpdateSubscibtionPlan
{
    public class UpdateSubscibtionPlanCommand : INotification
    {
        public int SellerId { get; set; }
        public int PlanId { get; set; }
        public string Title { get; set; }
        public decimal Price { get; set; }
        public CurrencyType CurrencyType { get; set; }
        public int Position { get; set; }
        public IList<int> PlanRoleIds { get; set; }
        public bool IsPaused { get; set; }

        public bool IsPublished { get; set; }

        public bool PayLocked { get; set; }

        public string BitrixGoodId { get; set; }


        public ICollection<ProductCustomizationField> CustomizationFields { get; set; }
    }

    public class UpdateSubscibtionPlanCommandHandler : INotificationHandler<UpdateSubscibtionPlanCommand>
    {
        private IATASDbContext _atasDbContext;
        private readonly IMediator _mediator;

        public UpdateSubscibtionPlanCommandHandler(IATASDbContext atasDbContext, IMediator mediator)
        {
            _atasDbContext = atasDbContext;
            _mediator = mediator;
        }

        public async Task Handle(UpdateSubscibtionPlanCommand notification, CancellationToken cancellationToken)
        {
            var plan = await _atasDbContext.SubscriptionPlans
                .Include(x => x.SubscriptionRoles)
                .FirstOrDefaultAsync(x => x.Id == notification.PlanId);
            if (plan is null)
                throw new NotFoundException(nameof(ProductPlan), notification.PlanId);

            var previousVersionPlan = plan.Clone();

            plan.Title = notification.Title;
            plan.UpdatePrice(new Money(notification.Price, notification.CurrencyType));
            
            plan.Position = notification.Position;
            plan.IsPaused = notification.IsPaused;
            plan.IsPublished = notification.IsPublished;
            plan.PayLocked = notification.PayLocked;
            plan.BitrixGoodId = notification.BitrixGoodId;
            plan.CustomizationFields = notification.CustomizationFields;
            plan.UpdatePlatformRoles(notification.PlanRoleIds);

            await _mediator.Publish(new ProductPlanModifiedEvent(previousVersionPlan, plan));

            await _atasDbContext.Save();
        }
    }
}