namespace Application
{
    public class Constants
    {
        public class EmailTemplateVariables
        {
            public const string SiteUrl = "{{site_url}}"; // ссылка на сайт
            public const string UserSubscriptionUrl = "{{user_subscription_url}}"; // ссылка емейл предпочтения
            public const string AffiliateProfileUrl = "{{affiliate_profile_url}}"; // ссылка на профиль партнера
            public const string UserRecoveryUrl = "{{user_recovery_url}}"; // ссылка восстановления пароля

            public const string
                UserConfirmPaymentUrl = "{{user_confirm_payment_url}}"; // ссылка подтверждения платежных реквизитов

            public const string
                RegUrlConfirmButton = "{{reg_url_confirm_button}}"; // URL ссылки Доступ в личный кабинет

            public const string UnsubscribeUrl = "{{unsubscribe_url}}"; // URL ссылки отписки
            public const string UserPartnerLink = "{{user_partner_link}}"; // ссылка // Доступ в личный кабинет партнера
            public const string ReferralLinks = "{{referral_links}}"; // Реферальные ссылки
            public const string UserEmail = "{{user_email}}"; // Email пользователя
            public const string UserName = "{{user_name}}"; // Логин пользователя
            public const string UserPasswordClean = "{{user_password_clean}}"; // Пароль пользователя
            public const string UserFirstname = "{{user_firstname}}"; // Имя пользователя
            public const string UserLastname = "{{user_lastname}}"; // Фамилия пользователя
            public const string UserPhonecode = "{{user_phonecode}}"; // Код телефона
            public const string UserUserphone = "{{user_userphone}}"; // Телефон пользователя
            public const string CartId = "{{cart_id}}"; // ID заказа
            public const string OrderIdInvoice = "{{order_id_invoice}}"; // INVOICE номер
            public const string CreditNoteId = "{{credit_note_id}}"; // CREDIT INVOICE номер
            public const string CartStatusType = "{{cart_status_type}}"; // статус заказа
            public const string ClientFirstName = "{{client_first_name}}"; // Имя пользователя в заказе
            public const string ClientLastName = "{{client_last_name}}"; // Фамилия пользователя в заказе
            public const string ClientEmail = "{{client_email}}"; // Email пользователя в заказе
            public const string ClientPhonecode = "{{client_phonecode}}"; // Код телефона в заказе
            public const string ClientPhone = "{{client_phone}}"; // Телефон пользователя в заказе
            public const string CartDatetimeAdd = "{{cart_datetime_add}}"; // Дата создания заказа
            public const string CartDatetimeUpd = "{{cart_datetime_upd}}"; // Дата изменения заказа
            public const string CartSumNoPromo = "{{cart_sum_no_promo}}"; // Сумма заказа без промокода
            public const string CartSumWithPromo = "{{cart_sum_with_promo}}"; // Сумма заказа c промокодом
            public const string CartSum = "{{cart_sum}}"; // Сумма заказа
            public const string PromoCode = "{{promo_code}}"; // Промокод в заказе
            public const string CartProductsList = "{{cart_products_list}}"; // Список товаров
            public const string WithdrawalId = "{{withdrawal_id}}"; // ID заявки на вывод средств
            public const string WithdrawalSum = "{{withdrawal_sum}}"; // Сумма заявки на вывод средств
            public const string PartnerFailText = "{{partner_fail_text}}"; // Причина отказа партнерства
        }
    }
}