using System;
using System.Collections.Generic;
using System.Linq;

namespace Application.Extensions
{
    public static class SortExtension
    {
        public static IEnumerable<TResult> SortBy<TResult, TKey>(
            this IEnumerable<TResult> itemsToSort,
            IEnumerable<TKey> sortKeys,
            Func<TResult, TKey> matchFunc)
        {
            return sortKeys.Join(itemsToSort,
                key => key,
                matchFunc,
                (key, iitem) => iitem);
        }
    }
}