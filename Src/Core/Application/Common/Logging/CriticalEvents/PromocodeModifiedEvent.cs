using Domain.BoundedContexts.Promotion.Aggregate;
using Domain.Enums;

namespace Application.Common.Logging.CriticalEvents
{
    public class PromocodeModifiedEvent : ObjectChangedEvent<Promocode>
    {
        public PromocodeModifiedEvent(Promocode previousVersion, Promocode currentVersion)
            : base(previousVersion.Code, previousVersion, currentVersion, previousVersion.Name)
        {
        }
    }

    
}