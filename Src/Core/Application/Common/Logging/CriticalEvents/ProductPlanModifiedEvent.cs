using Common.Abstractions;
using Domain.Abstractions;
using Domain.BoundedContexts.Selling.Abstractions;
using Domain.Enums;
using MediatR;

namespace Application.Common.Logging.CriticalEvents
{
    public class ProductPlanModifiedEvent : ObjectChangedEvent<ProductPlan>, INotification
    {
        public ProductPlanModifiedEvent(
            ProductPlan previousVersion,
            ProductPlan currentVersion) : base(previousVersion.Id,
            previousVersion, currentVersion, "Тариф")
        {
        }
    }
}