using Common.Abstractions;
using MediatR;

namespace Application.Common.Logging.CriticalEvents
{
    public class LoggerEvent : AuditableEvent, INotification
    {
        public LoggerEvent(string objectId, string objectType, string objectName, string actionDescription = null) :
            base(objectId, objectType, objectName, actionDescription)
        {
        }
    }
}