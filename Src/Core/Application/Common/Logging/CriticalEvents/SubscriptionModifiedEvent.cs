using Common.Abstractions;
using Domain.BoundedContexts.Subscriptions.Aggregate;

namespace Application.Common.Logging.CriticalEvents
{
    public class SubscriptionModifiedEvent : ObjectChangedEvent<Subscription>
    {
        public SubscriptionModifiedEvent(object id, Subscription previousVersion, Subscription currentVersion) :
            base(id, previousVersion, currentVersion, nameof(Subscription))
        {
        }
    }
}