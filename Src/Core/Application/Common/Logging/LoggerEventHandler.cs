using System.Threading;
using System.Threading.Tasks;
using Application.Common.Logging.CriticalEvents;
using Common.Abstractions;
using MediatR;

namespace Application.Common.Logging
{
    public class AuditableLoggingHanlder : INotificationHandler<LoggerEvent>
    {
        private ILoggerRepository _loggerRepository;

        public AuditableLoggingHanlder(ILoggerRepository loggerRepository)
        {
            _loggerRepository = loggerRepository;
        }

        public Task Handle(LoggerEvent notification, CancellationToken cancellationToken)
        {
            _loggerRepository.LogRows.Add(notification);
            return _loggerRepository.Save();
        }
    }
}