using System;
using System.Linq;
using System.Text;
using Application.Common.Logging.CriticalEvents;
using Common.Abstractions;
using Common.ValueObjects;
using Domain.Constants;
using Domain.Enums;
using Domain.ValueObjects;
using MediatR;

namespace Application.Common.Logging
{
    public class ObjectChangedEvent<T>  : LoggerEvent where T : IAuditableEntity
    {
        public EventActionType ActionType { get; }

        public ObjectChangedEvent(
            object id,
            T previousVersion,
            T currentVersion,
            string objectType)
            : base(id.ToString(), objectType, previousVersion.Name,
                FillChangeLogRows(previousVersion, currentVersion)) 
        {
            ActionType = EventActionType.Modified;
        }

        protected static string FillChangeLogRows(object previousVersion, object currentVersion)
        {
            var sb = new StringBuilder();

            sb.AppendLine($"{InternalLanguageConstants.Common.ObjectChanged}");

            foreach (var field in previousVersion.GetType().GetFields())
            {
                if (CriticalChangedTypes.All(type => type != field.FieldType))
                    continue;

                var previousValue = field.GetValue(previousVersion).ToString();
                var currentValue = field.GetValue(currentVersion).ToString();

                sb.AppendLine($"{field.Name} было: {previousValue}, стало: {currentValue}");
            }

            return sb.ToString();
        }

        //Since there may be null values in EF relationships, need to specify critical for change types,
        //anyway relationships reflected in integer values by id  
        private static readonly Type[] CriticalChangedTypes =
        {
            typeof(int),
            typeof(int?),
            typeof(string),
            typeof(Money),
            typeof(DateTime),
            typeof(DateTime?),
            typeof(bool),
            typeof(bool?),
            typeof(decimal),
            typeof(decimal?),
            typeof(Enum)
        };
    }
}