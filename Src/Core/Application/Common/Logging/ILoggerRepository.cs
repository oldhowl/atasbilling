using System.Threading.Tasks;
using Common.Abstractions;
using Microsoft.EntityFrameworkCore;

namespace Application.Common.Logging
{
    public interface ILoggerRepository
    {
        DbSet<AuditableEvent> LogRows { get; set; }

        public Task<int> Save();
    }
}