using Common.Abstractions;
using Domain.EventDispatcher;
using MediatR;

namespace Application.Common.DomainEvents
{
    public class DomainEventNotification<TDomainEvent> : INotification where TDomainEvent : IDomainEvent
    {
        public TDomainEvent DomainEventArgs { get; }

        public DomainEventNotification(TDomainEvent domainEventArgs)
        {
            DomainEventArgs = domainEventArgs;
        }
    }
}