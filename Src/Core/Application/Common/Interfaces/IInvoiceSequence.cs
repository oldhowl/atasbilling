using System.Threading.Tasks;

namespace Application.Common.Interfaces
{
    public interface IInvoiceSequence
    {
        Task<(string invoiceNextNum, string proformaInvoiceNum)> GetNext();
    }
}