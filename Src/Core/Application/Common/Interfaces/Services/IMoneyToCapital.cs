using Common.ValueObjects;
using Domain.ValueObjects;

namespace Application.Common.Interfaces.Services
{
    public interface IMoneyToCapitalService
    {
        public string MoneyToCapital(Money money);
    }
}