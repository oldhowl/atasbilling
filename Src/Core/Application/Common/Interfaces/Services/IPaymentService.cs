using System.Threading.Tasks;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.Enums;

namespace Application.Common.Interfaces.Services
{
    public interface IPaymentService
    {
        Task<string> GetPaymentUrl(Order order, PaymentType paymentType);
        Task Refund(Order order);
    }
}