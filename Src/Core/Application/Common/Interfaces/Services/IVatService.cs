using System.Threading.Tasks;
using Domain.ValueObjects;

namespace Application.Common.Interfaces.Services
{
    public interface IVatService
    {
        public Task<Vat> GetVatRateByNumber(string vatNumber);
    }
}