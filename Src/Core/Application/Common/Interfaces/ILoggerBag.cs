using System.Collections.Concurrent;
using Application.Common.Logging.CriticalEvents;
using Domain.EventDispatcher;

namespace Application.Common.Interfaces
{
    public interface ILoggerBag
    {
        IProducerConsumerCollection<LoggerEvent> LoggerEventsEvents { get; }

        void AddEvent(LoggerEvent @event);
    }
}