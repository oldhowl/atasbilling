using System.Threading;
using System.Threading.Tasks;
using Domain.BoundedContexts.Baskets.Aggregate;
using Domain.BoundedContexts.Customer.Aggregate;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.BoundedContexts.Owner.Aggregate;
using Domain.BoundedContexts.Promotion.Aggregate;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Domain.BoundedContexts.Subscriptions.Aggregate;
using Domain.BoundedContexts.Subscriptions.Entities;
using Microsoft.EntityFrameworkCore;

namespace Application.Common.Interfaces
{
    public interface IATASDbContext
    {
        DbSet<Order> Orders { get; set; }
        DbSet<Basket> Baskets { get; set; }
        DbSet<Customer> Customers { get; set; }
        DbSet<SubscriptionPlan> SubscriptionPlans { get; set; }
        DbSet<Subscription> Subscriptions { get; set; }
        DbSet<Owner> Owners { get; set; }
        DbSet<Promocode> Promocodes { get; set; }
        DbSet<Subscriber> Subscribers { get; set; }
        DbSet<Seller> Sellers { get; set; }
        DbSet<PlatformRole> PlatformRoles { get; set; }
        Task<int> Save(CancellationToken cancellationToken = default);

        public Task BeginTransaction();
        public void CommitTransaction();
    }
}