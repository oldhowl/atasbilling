using System;
using System.Collections.Generic;
using Common.Abstractions;
using Domain.Abstractions;
using Domain.BoundedContexts.Subscriptions.Models;
using Domain.Enums;
using Domain.EventDispatcher;

namespace Domain.BoundedContexts.Subscriptions.Events
{
    public class SubscriberSubscribedEvent : IDomainEvent
    {
        public Guid SubscriberId { get; }
        public IReadOnlyCollection<SubscriptionPlanDto> Subscriptions { get; }

        public SubscriberSubscribedEvent(IReadOnlyCollection<SubscriptionPlanDto> subscriptions, Guid subscriberId)
        {
            Subscriptions = subscriptions;
            SubscriberId = subscriberId;
        }
    }
}