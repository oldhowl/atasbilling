namespace Domain.BoundedContexts.Subscriptions.Events
{
    public class ExtendSubscriptionEvent
    {
        public int OrderItemId { get; set; }
        public int Days { get; set; }
    }
}