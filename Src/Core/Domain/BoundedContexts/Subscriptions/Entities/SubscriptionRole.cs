using Domain.BoundedContexts.Subscriptions.Aggregate;

namespace Domain.BoundedContexts.Subscriptions.Entities
{
    public class SubscriptionRole
    {
        
        public int SubscriptionId { get; private set; }
        public Subscription Subscription { get; private set; }

        public int PlatformRoleId { get; private set; }
        public PlatformRole PlatformRole { get; private set; }
        public SubscriptionRole(int subscriptionId, int platformRoleId)
        {
            SubscriptionId = subscriptionId;
            PlatformRoleId = platformRoleId;
        }

        private SubscriptionRole()
        {
            
        }

       
        
        
        
    }
}