using System.Collections.Generic;
using Common.Abstractions;

namespace Domain.BoundedContexts.Subscriptions.Entities
{
    public class PlatformRole : DomainEntity, IAuditableEntity
    {
        public int Id { get; set; }
        public string Title { get; private set; }
        public string DisplayName { get; private set; }

        public bool IsActive { get; private set; }

        public bool AllowsLogin { get; private set; }


        private List<SubscriptionRole> _subscriptionRoles = new List<SubscriptionRole>();
        public IReadOnlyCollection<SubscriptionRole> SubscriptionRoles => _subscriptionRoles;


        private List<Feature> _features = new List<Feature>();
        public IReadOnlyCollection<Feature> Features => _features;


        public PlatformRole(){}
        public PlatformRole(string title, string displayName)
        {
            Title = title;
            DisplayName = displayName;
            IsActive = true;
            AllowsLogin = true;
        }
        
        public PlatformRole(int id, string title, string displayName) : this(title,displayName)
        {
            Id = id;
        }

        public void AddFeatures(List<Feature> features)
        {
            _features.AddRange(features);
        }

        public override string ToString()
        {
            return Title;
        }

        public string Name => $"{nameof(PlatformRole)} {Title}";
    }
}