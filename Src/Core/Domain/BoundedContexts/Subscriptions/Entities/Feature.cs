using Common.Abstractions;

namespace Domain.BoundedContexts.Subscriptions.Entities
{
    public class Feature : DomainEntity, IAuditableEntity
    {
        public int Id { get; private set; }
        public string Title { get; private set; }
        public string Description { get; private set; }
        public bool IsActive { get; private set; }

        public Feature(string title, string description, bool isActive)
        {
            Title = title;
            Description = description;
            IsActive = isActive;
        }

        public Feature(int id, string title, string description, bool isActive) : this(title, description, isActive)
        {
            Id = id;
        }

        private Feature()
        {
        }


        public string Name => $"{nameof(Feature)} {Title}";
    }
}