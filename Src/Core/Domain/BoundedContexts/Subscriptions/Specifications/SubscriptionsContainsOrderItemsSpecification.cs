using System;
using System.Linq;
using System.Linq.Expressions;
using Domain.BoundedContexts.Subscriptions.Aggregate;

namespace Domain.BoundedContexts.Subscriptions.Specifications
{
    public class SubscriptionsContainsOrderItemsSpecification
    {
        public static Expression<Func<Subscription, bool>> Build(int[] subscriptionOrderItemIds)
        {
            return subscription
                => subscriptionOrderItemIds.All(id =>
                    subscription.SubscriptionPeriodExpansions.Any(spe => spe.OrderItemId == id));
        }
    }
}