using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.BoundedContexts.Subscriptions.Aggregate;

namespace Domain.BoundedContexts.Subscriptions.Specifications
{
    public class SubscriptionPeriodExpansionsContainsOrderItemIds
    {
        public static Expression<Func<SubscriptionPeriodExpansion, bool>> Build(IEnumerable<int> orderItemIds)
        {
            return periodExpansion => orderItemIds.Any(id => id == periodExpansion.SubscriptionId);
        }
    }
}