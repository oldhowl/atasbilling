using System;
using System.Collections.Generic;
using System.Linq;
using Common.ValueObjects;

namespace Domain.BoundedContexts.Subscriptions.Models
{
    public class SubscriptionBalance
    {
        public decimal BalanceAmount =>
            Math.Round(_infoPerPayment.Select(x => x.OrderItem.Price.Amount / x.DaysAdded * x.RestOfDays).Sum());

        private IEnumerable<SubscriptionOrderBalanceInfo> _infoPerPayment { get; }

        public SubscriptionBalance(IEnumerable<SubscriptionOrderBalanceInfo> infoPerPayment)
        {
            _infoPerPayment = infoPerPayment;
        }
    }
}