using System.Collections.Generic;
using Domain.Abstractions;
using Domain.Enums;

namespace Domain.BoundedContexts.Subscriptions.Models
{
    public class SubscriptionPlanDto
    {
        public int Id { get; }
        public int OrderItemId { get; }
        public string Name { get; }
        public int DaysAllowedToUse { get; }
        public SubscriptionType SubscriptionType { get; }
        public int ProductGroupId { get; }
        public int ProductId { get; }
        public IReadOnlyCollection<int> RoleIds { get; } 
        public IEnumerable<CustomizationField> CustomizationFields { get; }

        public SubscriptionPlanDto(int id, int orderItemId, string name, int daysAllowedToUse, SubscriptionType subscriptionType,int productId, int productGroupId, List<int> roleIds, IEnumerable<CustomizationField> customizationFields)
        {
            Id = id;
            RoleIds = roleIds;
            ProductId = productId;
            OrderItemId = orderItemId;
            Name = name;
            DaysAllowedToUse = daysAllowedToUse;
            SubscriptionType = subscriptionType;
            ProductGroupId = productGroupId;
            CustomizationFields = customizationFields;    
        }
    }
}