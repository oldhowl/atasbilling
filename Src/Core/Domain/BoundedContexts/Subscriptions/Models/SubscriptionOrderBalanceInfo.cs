using Domain.BoundedContexts.Orders.Aggregate;

namespace Domain.BoundedContexts.Subscriptions.Models
{
    public class SubscriptionOrderBalanceInfo
    {
        public int RestOfDays { get; }
        public OrderItem OrderItem { get; }
        public  int DaysAdded { get;  }
        public SubscriptionOrderBalanceInfo(int restOfDays, OrderItem orderItem, int daysAdded)
        {
            
            RestOfDays = restOfDays;
            OrderItem = orderItem;
            DaysAdded = daysAdded;
        }
        
         
    }
}