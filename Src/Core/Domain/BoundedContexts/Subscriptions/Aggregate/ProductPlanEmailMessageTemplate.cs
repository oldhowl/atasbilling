using Domain.Abstractions;
using Domain.BoundedContexts.Selling.Abstractions;

namespace Domain.BoundedContexts.Subscriptions.Aggregate
{
    public class ProductPlanEmailMessageTemplate : EmailMessageTemplate
    {
        public ProductPlan ProductPlan { get; set; }
    }
}