using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Abstractions;
using Domain.Abstractions;
using Domain.BoundedContexts.Baskets.Aggregate;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Domain.BoundedContexts.Subscriptions.Entities;
using Domain.BoundedContexts.Subscriptions.Exceptions;
using Domain.BoundedContexts.Subscriptions.Models;
using Domain.Enums;

namespace Domain.BoundedContexts.Subscriptions.Aggregate
{
    public class Subscription : DomainEntity, IAuditableEntity
    {
        public int Id { get; set; }
        public Guid SubscriberId { get; set; }
        public Subscriber Subscriber { get; set; }
        public string SubscriptionTitle { get; set; }
        public SubscriptionType SubscriptionType { get; set; }

        public bool IsAutoRenewal { get; private set; }
        public int ProductPlanId { get; private set; }
        public int ProductId { get; private set; }
        public int ProductGroupId { get; private set; }

        public IEnumerable<CustomizationField> CustomizationFields { get; set; }

        public ICollection<SubscriptionPeriodExpansion> SubscriptionPeriodExpansions { get; set; }
            = new List<SubscriptionPeriodExpansion>();

        public DateTimeOffset Created { get; private set; }
        public bool IsBlocked { get; private set; }

        public bool IsActive =>
            !IsBlocked
            && GetExpiration(true, false) > DateTimeOffset.Now;

        private List<SubscriptionRole> _subscriptionRoles = new List<SubscriptionRole>();
        public IReadOnlyCollection<SubscriptionRole> SubscriptionRoles => _subscriptionRoles;

        public ICollection<PausingRecord> Pausings { get; set; } = new List<PausingRecord>();
        public bool IsPaused => Pausings.Any(x => x.ReleaseDate.HasValue);


        public string Name => SubscriptionTitle;

        public Subscription(
            int productPlanId,
            int productId,
            int productGroupId,
            string title,
            SubscriptionType type,
            IReadOnlyCollection<int> roleIds,
            ICollection<CustomizationField> customizationFields,
            SubscriptionPeriodExpansion subscriptionPeriodExpansion)
        {
            _subscriptionRoles.AddRange(roleIds.Select(roleId => new SubscriptionRole(Id, roleId)));
            SubscriptionPeriodExpansions.Add(subscriptionPeriodExpansion);
            ProductPlanId = productPlanId;
            ProductId = productId;
            ProductGroupId = productGroupId;
            SubscriptionTitle = title;
            Created = DateTimeOffset.Now;
            CustomizationFields = customizationFields;
        }

        private Subscription()
        {
        }

        public void SwitchSubscriptionType(SubscriptionType subscriptionType)
        {
            SubscriptionType = subscriptionType;
        }

        public void ExtendSubscriptionDays(SubscriptionPeriodExpansion subscriptionPeriodExpansion)
        {
            SubscriptionPeriodExpansions.Add(subscriptionPeriodExpansion);
        }

        public void ReduceExpirationDays(SubscriptionPeriodExpansion subscriptionPeriodExpansion)
        {
            subscriptionPeriodExpansion.Block();
        }

        public void Pause()
        {
            if (!IsActive)
                throw new SubscriptionException("You can't pause inactive subscribe");

            if (!Pausings.Any(x => x.ReleaseDate.HasValue))
                throw new SubscriptionException("Subscription already paused");

            Pausings.Add(new PausingRecord());
        }

        public void Release()
        {
            var pausingRecord = Pausings.FirstOrDefault(x => !x.ReleaseDate.HasValue);
            if (pausingRecord is null)
                throw new SubscriptionException("Can not release unpaused subscription");

            pausingRecord.Release();
        }

        public void BlockSubbscription() => IsBlocked = true;
        public void UnblockSubbscription() => IsBlocked = false;

        public DateTimeOffset GetExpiration(bool includePauseDays, bool ignoreFreeDays)
        {
            DateTimeOffset? expiration = null;

            var expansions = SubscriptionPeriodExpansions.Where(x => !x.IsBlocked).ToList();
            if (!expansions.Any())
                return DateTimeOffset.Now.AddDays(-1);

            if (ignoreFreeDays)
                expansions = SubscriptionPeriodExpansions.Where(x => x.OrderItemId.HasValue).ToList();

            foreach (var expansion in expansions.OrderBy(x => x.Created))
                expiration = expiration?.AddDays(expansion.AddedDays) ?? expansion.Created.AddDays(expansion.AddedDays);

            if (Pausings.Any() && includePauseDays)
                expiration = expiration.Value.Add(Pausings.Select(x => x.GetPauseTime().Value)
                    .Aggregate((first, second) => first + second));

            // ReSharper disable once PossibleInvalidOperationException
            return expiration.Value.AddSeconds(-1);
        }

        public SubscriptionBalance GetBalanceInfo(IDateTime dateTime = null)
        {
            var subscriptionOrderBalanceInfos = new List<SubscriptionOrderBalanceInfo>();

            var dateTimeNow = dateTime?.Now ?? DateTimeOffset.Now;

            var expirationDays = (GetExpiration(false, true) - dateTimeNow).Days;
            foreach (var expansion in SubscriptionPeriodExpansions
                .Where(x => x.OrderItemId.HasValue)
                .OrderByDescending(x => x.Created))
            {
                expirationDays -= expansion.AddedDays;
                if (expirationDays > 0)
                    subscriptionOrderBalanceInfos.Add(
                        new SubscriptionOrderBalanceInfo(expansion.AddedDays, expansion.OrderItem,
                            expansion.AddedDays));
                else
                {
                    subscriptionOrderBalanceInfos.Add(new SubscriptionOrderBalanceInfo(
                        expirationDays + expansion.AddedDays,
                        expansion.OrderItem, expansion.AddedDays));
                    break;
                }
            }

            return subscriptionOrderBalanceInfos.Count > 0
                ? new SubscriptionBalance(subscriptionOrderBalanceInfos)
                : null;
        }

        public void UpdateRoles(ICollection<int> planProductRoleIds)
        {
            _subscriptionRoles.Clear();
            _subscriptionRoles.AddRange(planProductRoleIds.Select(platformRoleId =>
                new SubscriptionRole(Id, platformRoleId)));
        }
    }
}