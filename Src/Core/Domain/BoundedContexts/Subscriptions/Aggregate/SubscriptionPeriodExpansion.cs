using System;
using Domain.BoundedContexts.Orders.Aggregate;

namespace Domain.BoundedContexts.Subscriptions.Aggregate
{
    public class SubscriptionPeriodExpansion
    {
        public int Id { get; private set; }
        public int SubscriptionId { get; private set; }
        public Subscription Subscription { get; private set; }
        public int? OrderItemId { get; private set; }
        public OrderItem OrderItem { get; private set; }
        public bool IsBlocked { get; private set; }

        public DateTimeOffset? BlockDate { get; private set; }
        public int AddedDays { get; set; }
        public DateTimeOffset Created { get; private set; }


        public SubscriptionPeriodExpansion(int addedDays, int? orderItemId, DateTimeOffset payDate)
        {
            AddedDays = addedDays;
            Created = payDate;
            OrderItemId = orderItemId;
        }

        private SubscriptionPeriodExpansion()
        {
        }

        public void Block()
        {
            BlockDate = DateTimeOffset.Now;
            IsBlocked = true;
        }
    }
}