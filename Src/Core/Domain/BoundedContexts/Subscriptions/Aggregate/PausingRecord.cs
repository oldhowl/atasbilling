using System;

namespace Domain.BoundedContexts.Subscriptions.Aggregate
{
    public class PausingRecord
    {
        public DateTimeOffset? ReleaseDate { get; private set; }
        public DateTimeOffset? PauseDate { get; private set; }

        public PausingRecord()
        {
            PauseDate = DateTimeOffset.Now;
        }

        public TimeSpan? GetPauseTime()
        {
            if (!ReleaseDate.HasValue)
                return new TimeSpan();

            return (ReleaseDate - PauseDate);
        }

        public void Release() => ReleaseDate = DateTimeOffset.Now;
    }
}