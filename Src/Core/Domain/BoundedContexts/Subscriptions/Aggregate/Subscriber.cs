using System;
using System.Collections.Generic;
using System.Linq;
using Common.Abstractions;
using Domain.Abstractions;
using Domain.BoundedContexts.Subscriptions.Events;
using Domain.BoundedContexts.Subscriptions.Exceptions;
using Domain.BoundedContexts.Subscriptions.Models;
using Domain.EventDispatcher;

namespace Domain.BoundedContexts.Subscriptions.Aggregate
{
    public class Subscriber : DomainEntity, IAggregateRoot
    {
        public Guid Id { get; private set; }


        private List<Subscription> _subscriptions = new List<Subscription>();
        public IReadOnlyCollection<Subscription> Subscriptions => _subscriptions;


        public IReadOnlyCollection<Subscription> ActiveSubscriptions => _subscriptions.Where(x => x.IsActive).ToList();

        public Subscriber(Guid guid)
        {
            Id = guid;
        }

        private Subscriber()
        {
        }


        public void Subscribe(IReadOnlyCollection<SubscriptionPlanDto> subscriptionPlans, DateTimeOffset payDate)
        {
            foreach (var plan in subscriptionPlans)
            {
                //Юзер имеет текущую подписку. Проверяем, совпадает ли продукт(CME,MOEX)
                var existedSubscription =
                    Subscriptions
                        .FirstOrDefault(x =>
                            x.ProductPlanId == plan.Id
                            && x.IsActive);

                //Создаем новый период подписки
                var subscriptionPeriodExpansion =
                    new SubscriptionPeriodExpansion(plan.DaysAllowedToUse, plan.OrderItemId, payDate);

                // Если есть подписка на данный план
                if (existedSubscription != null)
                {
                    existedSubscription.SwitchSubscriptionType(plan.SubscriptionType);
                    existedSubscription.ExtendSubscriptionDays(subscriptionPeriodExpansion);
                    return;
                }

                //Юзер имеет подписку в данной группе
                //Сюда попадаем в кейсе, когда юзер покупает другой продукт одной и той же группы. Включаем механизм апгрейда
                var existedProductInGroup =
                    Subscriptions
                        .FirstOrDefault(x =>
                            x.ProductGroupId == plan.ProductGroupId);

                existedProductInGroup?.BlockSubbscription();

                //У юзера отсутствует подписка в группе или в принципе
                var newSubscription = new Subscription(
                    plan.Id,
                    plan.ProductId,
                    plan.ProductGroupId,
                    plan.Name,
                    plan.SubscriptionType,
                    plan.RoleIds,
                    plan.CustomizationFields?.ToList(),
                    subscriptionPeriodExpansion
                );

                _subscriptions.Add(newSubscription);
            }

            PublishEvent(new SubscriberSubscribedEvent(subscriptionPlans.ToList(), Id));
        }

        /// <summary>
        /// Get upgrade balance if exist
        /// </summary>
        /// <param name="productPlanId">New plan Id</param>
        /// <param name="productId">New plan product ID</param>
        /// <param name="productGroupId">New Product group Id</param>
        /// <returns>List of order ids and rest days or null</returns>
        public SubscriptionBalance GetDiscountUpgradeBalance(int productPlanId, int productId,
            int productGroupId)
        {
            if (Subscriptions.Any(x => x.ProductPlanId == productPlanId))
                return null;

            var upgradableSubscription = Subscriptions.FirstOrDefault(x =>
                x.ProductGroupId == productGroupId
                && x.ProductId != productId
                && x.IsActive);

            return upgradableSubscription?.GetBalanceInfo();
        }

        public void ReduceSubscriptionsAfterRefund(int[] subscriptionOrderItemIds)
        {
            var subscriptions =
                from orderItemId in subscriptionOrderItemIds
                join expiration in _subscriptions.SelectMany(x => x.SubscriptionPeriodExpansions)
                    on orderItemId equals expiration.OrderItemId
                group expiration by expiration.Subscription;

            foreach (var subscription in subscriptions)
            foreach (var subscriptionPeriodExpansion in subscription)
                subscription.Key.ReduceExpirationDays(subscriptionPeriodExpansion);
        }
    }
}