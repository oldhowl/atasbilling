using System.Collections.Generic;

namespace Domain.BoundedContexts.Subscriptions.Exceptions
{
    public class SubscriptionExpirationReduceException : SubscriptionException
    {
        public SubscriptionExpirationReduceException(IEnumerable<int> orderItemIds) 
            : base($"Subscription expansions for order items {string.Join(", ", orderItemIds)} not found!")
        {
        }
    }
}