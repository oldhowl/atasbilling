namespace Domain.BoundedContexts.Subscriptions.Exceptions
{
    public class SubscriptionExpirationAddException : SubscriptionException
    {
        public SubscriptionExpirationAddException(int planId, string planName) : base(
            $"Plan {planName} {planId} is not contain in order items")
        {
        }
    }
}