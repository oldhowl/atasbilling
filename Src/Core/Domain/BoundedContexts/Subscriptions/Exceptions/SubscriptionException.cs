using System;

namespace Domain.BoundedContexts.Subscriptions.Exceptions
{
    public class SubscriptionException : Exception
    {
        public SubscriptionException(string message) : base(message)
        {
        }
    }
}