using System;
using System.Text;
using Domain.Abstractions;

namespace Domain.BoundedContexts
{
    public class Individual : Entity
    {
        public string FirstName { get; set; }
        public string Surname { get; set; }
        public DateTime Birthdate { get; set; }
        

        public Individual(string name, string surname, DateTime birthdate)
        {
            FirstName = name;
            Surname = surname;
            Birthdate = birthdate;
        }

        public override string FullName()
        {
            var sb = new StringBuilder();
            sb.AppendLine($"{Name} {Surname}");
            sb.AppendLine($"Birthdate: {Birthdate.Date.ToString("d")}");
            return sb.ToString();
        }

        public override string FullAddress()
        {
            throw new NotImplementedException();
        }

        public override string Name => $"{FirstName} {Surname}";
    }
}