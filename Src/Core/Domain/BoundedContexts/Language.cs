using Common.Enums;

namespace Domain.BoundedContexts
{
    public class Language
    {
        public int LanguageId { get; set; }
        public string Abbreviation { get; set; }
        public string Title { get; set; }
        public string PicPath { get; set; }
        public CurrencyType CurrencyType { get; set; }
    }
}