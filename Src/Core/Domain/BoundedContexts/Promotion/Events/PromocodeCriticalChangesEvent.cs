using System;
using Common.Abstractions;
using Domain.Abstractions;

namespace Domain.BoundedContexts.Promotion.Events
{
    public class PromocodeCriticalChangesEvent : EventBase
    {
        public string Code { get; }
        public bool IsPercentDiscount { get; }
        public decimal DiscountAmount { get; }
        public int[] ProductPlanIds { get; }
        public DateTimeOffset? EndDate { get; }
        

        public PromocodeCriticalChangesEvent(string code,bool isPercentDiscount, decimal discountAmount, int[] productPlanIds, DateTimeOffset? endDate)
        {
            Code = code;
            IsPercentDiscount = isPercentDiscount;
            DiscountAmount = discountAmount;
            ProductPlanIds = productPlanIds;
            EndDate = endDate;
        }
    }
}