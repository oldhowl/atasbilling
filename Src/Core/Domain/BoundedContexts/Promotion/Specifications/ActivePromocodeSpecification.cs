using System;
using System.Linq.Expressions;
using Domain.BoundedContexts.Promotion.Aggregate;

namespace Domain.BoundedContexts.Promotion.Specifications
{
    public class ActivePromocodeSpecification
    {
        public static Expression<Func<Promocode, bool>> Build(string code)
        {
            return promocode =>
                !(promocode.EndDate.HasValue && promocode.EndDate < DateTimeOffset.Now
                  || promocode.MaxUseCount < 0
                  || !promocode.IsActive)
                && promocode.Code == code;
        }
    }
}