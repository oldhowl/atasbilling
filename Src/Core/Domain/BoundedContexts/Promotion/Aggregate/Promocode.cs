using System;
using System.Collections.Generic;
using System.Linq;
using Common.Abstractions;
using Common.ValueObjects;
using Domain.Abstractions;
using Domain.BoundedContexts.Promotion.Enums;

namespace Domain.BoundedContexts.Promotion.Aggregate
{
    public class Promocode : DomainEntity, IAuditableEntity, IAggregateRoot
    {
        public string Code { get; private set; }

        public int MaxUseCount { get; set; }
        public DiscountType DiscountType { get; set; }
        public decimal DiscountAmount { get; set; }

        public bool IsActive { get; set; }
        public string Description { get; set; }

        public bool IsPriority { get; set; }

        public DateTimeOffset StartDate { get; set; }
        public DateTimeOffset? EndDate { get; set; }

        public ICollection<int> ProductPlanIds { get; private set; } = new List<int>();
        public string Name => $"{GetType().Name} {Code}";

        public Promocode(
            string code, List<int> productPlanIds, int maxUseCount, DiscountType discountType,
            decimal discountAmount,
            string description, bool isPriority, DateTimeOffset startDate, DateTimeOffset? endDate)
        {
            ProductPlanIds = productPlanIds;
            Code = code.ToUpper();
            MaxUseCount = maxUseCount;
            DiscountType = discountType;
            DiscountAmount = discountAmount;
            Description = description;
            IsPriority = isPriority;
            StartDate = startDate;
            EndDate = endDate;
        }

        private Promocode()
        {
        }

        public void UpdateProductPlans(IEnumerable<int> productPlanIds)
        {
            ProductPlanIds = productPlanIds.ToList();
        }


        public void Used()
        {
            MaxUseCount--;
        }

        public Money CalculateDiscountPrice(Money money)
        {
            var price =
                DiscountType == DiscountType.Percent
                    ? money.Amount -= money.Amount / 100 * DiscountAmount
                    : money.Amount - DiscountAmount;

            if (money.Amount <= 0)
                money.Amount = 0;

            return new Money(price, money.Currency);
        }

        public decimal CalculateDiscountPrice(decimal price)
        {
            var discountPrice =
                DiscountType == DiscountType.Percent
                    ? price / 100 * DiscountAmount
                    : DiscountAmount;

            if (discountPrice <= 0)
                discountPrice = 0;

            return discountPrice;
        }
    }
}