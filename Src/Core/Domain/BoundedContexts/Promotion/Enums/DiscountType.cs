namespace Domain.BoundedContexts.Promotion.Enums
{
    public enum DiscountType
    {
        Percent,
        Amount
    }
}