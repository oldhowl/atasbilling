using System;
using System.Collections.Generic;
using Common.Abstractions;
using Domain.Abstractions;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;

namespace Domain.BoundedContexts.Selling.Abstractions
{
    public class Product : DomainEntity, IAuditableEntity 
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int Position { get; set; }
        public bool IsPublished { get; set; }
        
        public Guid SellerId { get; set; }
        
        public int ProductGroupId { get; set; }
        public ProductGroup ProductGroup { get; set; }

        public ICollection<ProductTranslationDescription> ProductTranslationDescriptions { get; set; }

        protected List<ProductPlan> _productPlans = new List<ProductPlan>();
        public IReadOnlyCollection<ProductPlan> Plans => _productPlans;

        public ICollection<ProductCustomizationField> ProductCustomizationFieldsTemplate { get; set; }
        public Product(string title, int position, bool isPublished, Guid sellerId)
        {
            Title = title;
            Position = position;
            IsPublished = isPublished;
            SellerId = sellerId;
        }

        public string Name => Title;
    }
}