using System.Collections.Generic;
using Common.Abstractions;
using Common.ValueObjects;
using Domain.Abstractions;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;

namespace Domain.BoundedContexts.Selling.Abstractions
{
    public abstract class ProductPlan : DomainEntity, IAuditableEntity 
    {
        protected ProductPlan(string title, Money price, Money oldPrice, int position, bool isPaused, bool isPublished, bool payLocked, string bitrixGoodId, ICollection<ProductPlanTranslationDescription> productLanguageDescriptions, ICollection<ProductCustomizationField> customizationFields)
        {
            Title = title;
            Price = price;
            OldPrice = oldPrice;
            Position = position;
            IsPaused = isPaused;
            IsPublished = isPublished;
            PayLocked = payLocked;
            BitrixGoodId = bitrixGoodId;
            ProductLanguageDescriptions = productLanguageDescriptions;
            CustomizationFields = customizationFields;
        }
        
        public ProductPlan(){}

        public abstract int Id { get; set; }
        public int ProductId { get; set; }
        public Product Product { get; set; }
        public string Title { get; set; }
        public Money Price { get; set; }
        
        public Money OldPrice { get; set; }

        public int Position { get; set; }
        
        public bool IsPaused { get; set; }
        
        public bool IsPublished { get; set; }
        
        public bool PayLocked { get; set; }

        public string BitrixGoodId { get; set; }
        
        public ICollection<ProductPlanTranslationDescription> ProductLanguageDescriptions { get; set; }
        
        public int? EmailMessageTemplateId { get; set; }
        
        public EmailMessageTemplate EmailMessageTemplate { get; set; }
        
        public string FullPlanName => $"{Product.Title} / {Title}";

        public ICollection<ProductCustomizationField> CustomizationFields { get; set; }


        public  string Name => Title;
    }
}