using System;
using System.Collections.Generic;
using Common.Abstractions;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;

namespace Domain.BoundedContexts.Selling.Abstractions
{
    public class ProductGroup : DomainEntity, IAuditableEntity
    {
        public int Id { get; set; }
        public string Title { get; private set; }
        public Guid SellerId { get; private set; }
        public Seller Seller { get; private set; }

        protected List<Product> _products = new List<Product>();
        public IReadOnlyCollection<Product> Products => _products; 

        public ProductGroup(Guid guid,string title) : this(title)
        {
            SellerId = guid;
        }

        public ProductGroup(string title)
        {
            Title = title;
        }
        
        private ProductGroup(){}

        public void AddProduct(Product product)
        {
            _products.Add(product);
        }
        
        

        public string Name => Title;
    }
}