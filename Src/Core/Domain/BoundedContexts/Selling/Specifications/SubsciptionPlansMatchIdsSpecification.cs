using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;

namespace Domain.BoundedContexts.Selling.Specifications
{
    public class SubsciptionPlansMatchIdsSpecification
    {
        public static Expression<Func<SubscriptionPlan, bool>> Build(IEnumerable<int> ids)
        {
            return subscriptionPlan => ids.Contains(subscriptionPlan.Id);
        }
    }
}