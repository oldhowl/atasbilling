using Common.Abstractions;
using Common.ValueObjects;
using Domain.Abstractions;
using Domain.EventDispatcher;
using Domain.ValueObjects;

namespace Domain.BoundedContexts.Selling.Events
{
    public class PlanPriceChangedEvent : IDomainEvent
    {
        public int PlanId { get; }
        public Money NewPrice { get; }

        public PlanPriceChangedEvent(int planId, Money newPrice)
        {
            PlanId = planId;
            NewPrice = newPrice;
        }
    }
}