using System.Collections.Generic;
using Common.Abstractions;
using Domain.EventDispatcher;

namespace Domain.BoundedContexts.Selling.Events
{
    public class SubscriptionPlanRolesChangedEvent : IDomainEvent
    {
        public int PlanId { get; }
        public ICollection<int> PlanProductRoleIds { get; }

        public SubscriptionPlanRolesChangedEvent(int planId, ICollection<int> planProductRoleIds)
        {
            PlanId = planId;
            PlanProductRoleIds = planProductRoleIds;
        }
    }
}