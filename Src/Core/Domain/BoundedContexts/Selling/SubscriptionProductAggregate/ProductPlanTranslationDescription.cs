
namespace Domain.BoundedContexts.Selling.SubscriptionProductAggregate
{
    public class ProductPlanTranslationDescription
    {
        public int Id { get; set; }
        public int LanguageId { get; set; }
        public string PublicTitle { get; set; }
        public string InvoiceTitle { get; set; }
        public string WidgetBlockTitle { get; set; }
        public string WidgetButtonTitle { get; set; }
        public string FirstWidgetBlockSubtitle { get; set; }
        public string SecondWidgetBlockSubtitle { get; set; }
        public string ShortDescription { get; set; }
        public string FullDescription { get; set; }

    }
}