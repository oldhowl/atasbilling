using System.Collections.Generic;
using Domain.Abstractions;

namespace Domain.BoundedContexts.Selling.SubscriptionProductAggregate
{
    /// <summary>
    /// Кастомное техническое поле подписки
    /// </summary>
    public class ProductCustomizationField : CustomizationField
    {
        /// <summary>
        /// Переводы
        /// </summary>
        public ICollection<ProductCustomizationFieldTranslation> Translations { get; set; }

        public CustomizationField GetBase() => this;
    }
}