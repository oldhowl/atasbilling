using System;
using System.Collections.Generic;
using Common.Abstractions;
using Common.ValueObjects;
using Domain.Abstractions;
using Domain.BoundedContexts.Selling.Abstractions;
using Domain.ValueObjects;

namespace Domain.BoundedContexts.Selling.SubscriptionProductAggregate
{
    public class Seller
    {
        public Guid Id { get; private set; }
        public string Name { get; private set; }
        public Requisites SellerRequisits { get; private set; }
        public Person Representative { get; private set; }

        private List<ProductGroup> _groups = new List<ProductGroup>();
        public IReadOnlyCollection<ProductGroup> ProductGroups => _groups;

        public Seller(Guid guid)
        {
            Id = guid;
        }

        private Seller()
        {
            //EF
        }

        public Seller(string name, Requisites sellerRequisits, Person representative)
        {
            Name = name;
            SellerRequisits = sellerRequisits;
            Representative = representative;
        }

        public void AddProductGroup(ProductGroup productGroup)
        {
            _groups.Add(productGroup);
        }
    }
}