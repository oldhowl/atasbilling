using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using Common.ValueObjects;
using Domain.Abstractions;
using Domain.BoundedContexts.Selling.Abstractions;
using Domain.BoundedContexts.Selling.Events;
using Domain.BoundedContexts.Subscriptions.Entities;
using Domain.Enums;

namespace Domain.BoundedContexts.Selling.SubscriptionProductAggregate
{
    public class SubscriptionPlan : Abstractions.ProductPlan
    {
        public int DaysAllowedToUse { get; private set; }

        private List<PlatformRole> _subscriptionRoles = new List<PlatformRole>();
        public IReadOnlyCollection<PlatformRole> SubscriptionRoles => _subscriptionRoles;
        public override int Id { get; set; }
        public ICollection<ProductPlanTranslationDescription> ProductLanguageDescriptions { get; private set; }
        public SubscriptionType SubscriptionType { get; private set; }

        public SubscriptionPlan() : base()
        {
            
        }
        public SubscriptionPlan(
            string title,
            Money price,
            Money oldPrice,
            int position,
            bool isPaused,
            bool isPublished,
            bool payLocked,
            string bitrixGoodId,
            ICollection<ProductPlanTranslationDescription> productLanguageDescriptions,
            ICollection<ProductCustomizationField> customizationFields,
            int daysAllowedToUse,
            SubscriptionType subscriptionType,
            List<PlatformRole> platformRoles) :
            base(
                title,
                price,
                oldPrice,
                position,
                isPaused,
                isPublished,
                payLocked,
                bitrixGoodId,
                productLanguageDescriptions,
                customizationFields)
        {
            _subscriptionRoles = platformRoles;
            DaysAllowedToUse = daysAllowedToUse;
            ProductLanguageDescriptions = productLanguageDescriptions;
            SubscriptionType = subscriptionType;
        }



        public void UpdatePrice(Money newPrice)
        {
            if (Price == newPrice)
                return;
            
            Price = newPrice;
            PublishEvent(new PlanPriceChangedEvent(Id, Price));
        }

        public void UpdatePlatformRoles(IList<int> newPlanPlatformRoles)
        {
            _subscriptionRoles.Clear();
            _subscriptionRoles.AddRange(newPlanPlatformRoles.Select(x => new PlatformRole {Id = x}));
            PublishEvent(new SubscriptionPlanRolesChangedEvent(Id, newPlanPlatformRoles));
        }
    }
}