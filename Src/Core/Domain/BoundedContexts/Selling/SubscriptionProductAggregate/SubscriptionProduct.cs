using System;
using Domain.Abstractions;
using Domain.BoundedContexts.Selling.Abstractions;

namespace Domain.BoundedContexts.Selling.SubscriptionProductAggregate
{
    public class SubscriptionProduct : Product
    {
        public SubscriptionProduct(string title, int position, bool isPublished, Guid sellerId) : base(title, position,
            isPublished, sellerId)
        {
        }


        public void AddPlan(SubscriptionPlan subsPlan)
        {
            _productPlans.Add(subsPlan);
        }
    }
}