namespace Domain.BoundedContexts.Selling.SubscriptionProductAggregate
{
    public class ProductTranslationDescription
    {
        public int Id { get; set; }
        public int LanguageId { get; set; }
        public Language Language { get; set; }
        public string MessengerTitle { get; set; }
        public string MessengerDescription { get; set; }
        public string PublicTitle { get; set; }
        public string InvoiceTitle { get; set; }
        public string ShortPublicDescription { get; set; }
        public string PublicDescription { get; set; }

       
    }
}