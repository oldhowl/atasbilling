using System;
using Domain.Abstractions;
using Domain.BoundedContexts.Selling.Abstractions;

namespace Domain.BoundedContexts.Selling.SubscriptionProductAggregate
{
    public class SubscriptionProductsGroup : ProductGroup
    {
        public SubscriptionProductsGroup(Guid guid, string title) : base(guid, title)
        {
        }

        public SubscriptionProductsGroup(string title) : base(title)
        {
        }
    }
}