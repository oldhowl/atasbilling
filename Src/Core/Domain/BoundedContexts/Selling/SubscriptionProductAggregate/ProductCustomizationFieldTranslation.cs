namespace Domain.BoundedContexts.Selling.SubscriptionProductAggregate
{
    public class ProductCustomizationFieldTranslation
    {
        public string Language { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}