using System;
using Common.Abstractions;
using Domain.Abstractions;

namespace Domain.BoundedContexts.Affilate.Partners.Aggregate
{
    public class Partner : DomainEntity,IAuditableEntity, IAggregateRoot
    {
        public Guid Id { get; private set; }
        public string Name { get; }
        
    }
}