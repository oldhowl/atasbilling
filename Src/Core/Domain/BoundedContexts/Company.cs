using System;
using System.Text;
using Domain.Abstractions;

namespace Domain.BoundedContexts
{
    public class Company : Entity
    {
        public string CompanyName { get; set; }
        public string EntityType { get; set; }
        public string RegistrationNumber { get; set; }
        public string VatNumber { get; set; }


        public override string FullName()
        {
            var sb = new StringBuilder();

            sb.AppendLine($"{Name} {EntityType}");
            sb.AppendLine($"Reg. № {RegistrationNumber}");
            if (!string.IsNullOrEmpty(VatNumber))
                sb.AppendLine($"VAT #: {VatNumber}");

            return sb.ToString();
        }

        public override string FullAddress()
        {
            throw new NotImplementedException();
        }

        public override string Name => CompanyName;
    }
}