using System;

namespace Domain.BoundedContexts.Orders.Exceptions
{
    public class OrderStatusException : Exception
    {
        public OrderStatusException(string message) : base(message)
        {
            
        }
    }
}