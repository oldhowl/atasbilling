using System;

namespace Domain.BoundedContexts.Orders.Exceptions
{
    public class ProductPlanCantBeAddedToBasketException : Exception
    {
        private readonly string _planTitle;

        public ProductPlanCantBeAddedToBasketException(string planTitle)
        {
            _planTitle = planTitle;
        }
    }
}