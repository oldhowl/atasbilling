using System;

namespace Domain.BoundedContexts.Orders.Exceptions
{
    public class OrderCannotBeRefundedException  : Exception
    {
        public OrderCannotBeRefundedException(string message) : base(message)
        {
            
        }
    }
}