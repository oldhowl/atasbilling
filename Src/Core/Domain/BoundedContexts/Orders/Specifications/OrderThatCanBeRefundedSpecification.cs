using System;
using System.Linq;
using System.Linq.Expressions;
using Domain.BoundedContexts.Orders.Aggregate;

namespace Domain.BoundedContexts.Orders.Specifications
{
    public class OrderThatCanBeRefundedSpecification
    {
        public static Expression<Func<Order, bool>> Build(int orderId)
        {
            return order =>
                order.PaymentInfo.PayDate > DateTimeOffset.Now.AddDays(-14);
        }
    }
}