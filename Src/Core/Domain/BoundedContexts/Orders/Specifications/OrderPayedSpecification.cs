using System;
using System.Linq.Expressions;
using Domain.BoundedContexts.Orders.Aggregate;

namespace Domain.BoundedContexts.Orders.Specifications
{
    public class OrderPayedSpecification
    {
        public static Expression<Func<Order, bool>> ByCustomerId(Guid customerId)
        {
            return x =>
                x.CustomerId == customerId
                && x.PaymentInfo.PayDate != null && !(x.PaymentInfo.RefundDate != null);
        }
    }
}