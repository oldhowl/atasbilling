using System;
using System.Collections.Generic;
using System.Linq;
using Common.Abstractions;
using Common.ValueObjects;
using Domain.Abstractions;

namespace Domain.BoundedContexts.Orders.Aggregate
{
    public class Invoice : DomainEntity, IAuditableEntity
    {
        public int Id { get; private set; }
        public string Number { get; private set; }
        public Requisites BillTo { get; private set; }
        public DateTime DueDate { get;private  set; }
        public string Currency { get;private  set; }
        public decimal TotalAmount { get; private set; }
        public string TotalAmountText { get; private set; }
        public BankRequsites Requsites { get; private set; }
        public List<InvoiceItem> InvoiceItems { get; private set; }
        public string Name => $"{nameof(Order)} {Number}";

        public Invoice(string number, Requisites billTo, DateTime dueDate, string currency, decimal totalAmount,
            string totalAmountText, BankRequsites requsites, IEnumerable<InvoiceItem> invoiceItems)
        {
            Number = number;
            BillTo = billTo;
            DueDate = dueDate;
            Currency = currency;
            TotalAmount = totalAmount;
            TotalAmountText = totalAmountText;
            Requsites = requsites;
            InvoiceItems = invoiceItems.ToList();
        }

        
        private Invoice(){}
    }

    public class InvoiceItem
    {
        public string ProductName { get; private set; }
        public string Qty { get; private set; }
        public decimal Price { get; private set; }
        public decimal Amount { get; private set; }

        public InvoiceItem(string productName, string qty, decimal price, decimal amount)
        {
            ProductName = productName;
            Qty = qty;
            Price = price;
            Amount = amount;
        }

        private InvoiceItem(){}
    }
}