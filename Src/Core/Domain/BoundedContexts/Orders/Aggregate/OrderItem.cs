using System;
using Common;
using Common.Abstractions;
using Common.ValueObjects;
using Domain.Abstractions;

namespace Domain.BoundedContexts.Orders.Aggregate
{
    public class OrderItem : DomainEntity, IAuditableEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int ProductPlanId { get; set; }

        public string PositionName { get; private set; }
        public Money Price { get; set; }


        public string Name => $"Purchase id: {Id}";

        public OrderItem(int productPlanId, int productId, string name, Money price)
        {
            ProductPlanId = productPlanId;
            PositionName = name;
            ProductId = productId;
            Price = price;
        }

        protected OrderItem()
        {
        }


        public InvoiceItem ToInvoiceItem()
        {
            return new InvoiceItem(PositionName, "1", Price.Amount, Price.Amount);
        }
    }
}