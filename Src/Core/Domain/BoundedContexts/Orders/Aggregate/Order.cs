using System;
using System.Collections.Generic;
using System.Linq;
using Common;
using Common.Abstractions;
using Common.ValueObjects;
using Domain.Abstractions;
using Domain.BoundedContexts.Orders.Events;
using Domain.BoundedContexts.Orders.Exceptions;
using Domain.BoundedContexts.Orders.ValueObjects;
using Domain.Enums;
using Domain.ValueObjects;

namespace Domain.BoundedContexts.Orders.Aggregate
{
    public class Order : DomainEntity, IAuditableEntity, IAggregateRoot
    {
        #region Columns

        public int Id { get; set; }

        public string Promocode { get; private set; }
        public DateTimeOffset Created { get; private set; }
        public Guid CustomerId { get; private set; }
        public Customer.Aggregate.Customer Customer { get; private set; }

        private List<OrderItem> _items = new List<OrderItem>();
        public IReadOnlyCollection<OrderItem> Items => _items;

        public int? InvoiceId { get; private set; }
        public Invoice Invoice { get; private set; }

        public int? ProformaInvoiceId { get; private set; }
        public Invoice ProformaInvoice { get; private set; }


        #region VO

        public Vat Vat { get; private set; }

        public PaymentInfo PaymentInfo { get; private set; }

        #endregion

        #endregion

        public string Name => $"{nameof(Order)} {Id}";

        //EF default ctor
        private Order()
        {
        }


        public Order(
            Guid customerId,
            List<OrderItem> orderItems,
            Vat vat,
            string promocode,
            Requisites requisites,
            PaymentType paymentType,
            IDateTime dateTime = null)
        {
            PaymentInfo = new PaymentInfo(paymentType, requisites,
                orderItems.Select(x => x.Price).Aggregate((m1, m2) => m1 + m2));
            CustomerId = customerId;
            _items = orderItems;
            Vat = vat;
            Created = dateTime?.Now ?? DateTimeOffset.Now;
            Promocode = promocode;
        }


        public OrderStatus OrderStatus
        {
            get
            {
                if (PaymentInfo.IsPayed && !PaymentInfo.RefundDate.HasValue)
                    return OrderStatus.Payed;

                if (PaymentInfo.RefundDate.HasValue)
                    return OrderStatus.PaymentRefund;

                if (!PaymentInfo.PayDate.HasValue)
                    return OrderStatus.WaitingPayment;

                throw new OrderStatusException($"Impossible order status, check order {Id}");
            }
        }

        public void Payed(
            string invoiceNextNum,
            string proformaNextNum,
            Func<Money, string> moneyToText,
            BankRequsites ownerBankRequisites, IDateTime dateTime = null)
        {
            var payDate = PaymentInfo.Payed(dateTime);

            Invoice = new Invoice(
                invoiceNextNum,
                PaymentInfo.Requisites,
                new DateTime(),
                PaymentInfo.TotalPrice.Currency.IsoCode.ToUpper(),
                PaymentInfo.TotalPrice.Amount,
                moneyToText?.Invoke(PaymentInfo.TotalPrice),
                ownerBankRequisites,
                _items.Select(x => x.ToInvoiceItem()));


            ProformaInvoice = new Invoice(
                proformaNextNum,
                PaymentInfo.Requisites,
                new DateTime(),
                PaymentInfo.TotalPrice.Currency.IsoCode.ToUpper(),
                PaymentInfo.TotalPrice.Amount,
                moneyToText?.Invoke(PaymentInfo.TotalPrice),
                ownerBankRequisites,
                _items.Select(x => x.ToInvoiceItem()));


            PublishEvent(
                new OrderPayedEvent(
                    payDate,
                    CustomerId,
                    Items.ToList()));
        }

        public void Refund()
        {
            PaymentInfo.Refund();
            this.PublishEvent(new OrderRefundedEvent(CustomerId, Items.Select(x => x.Id).ToArray()));
        }
    }
}