using System;
using System.Collections.Generic;
using Common.Abstractions;
using Domain.Abstractions;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.EventDispatcher;

namespace Domain.BoundedContexts.Orders.Events
{
    public class OrderPayedEvent : IDomainEvent
    {
        public DateTimeOffset PayedDate { get; }
        public Guid CustomerId { get; }
        public IReadOnlyCollection<OrderItem> SubscriptionOrderItems { get; }

        public OrderPayedEvent(DateTimeOffset payedDate, Guid customerId, IReadOnlyCollection<OrderItem> subscriptionOrderItems)
        {
            PayedDate = payedDate;
            CustomerId = customerId;
            SubscriptionOrderItems = subscriptionOrderItems;
        }
    }
}