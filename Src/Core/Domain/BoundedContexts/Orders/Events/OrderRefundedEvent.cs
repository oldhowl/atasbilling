using System;
using Common.Abstractions;
using Domain.EventDispatcher;

namespace Domain.BoundedContexts.Orders.Events
{
    public class OrderRefundedEvent : IDomainEvent
    {
        public Guid CustomerId { get; }
        public int[] SubscriptionOrderItemIds { get; private set; }

        public OrderRefundedEvent(Guid customerId, int[] subscriptionOrderItemIds) : base()
        {
            SubscriptionOrderItemIds = subscriptionOrderItemIds;
            CustomerId = customerId;
        }
    }
}