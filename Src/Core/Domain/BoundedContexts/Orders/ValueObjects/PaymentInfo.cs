using System;
using System.Collections.Generic;
using Common;
using Common.ValueObjects;
using Domain.BoundedContexts.Orders.Exceptions;
using Domain.Enums;
using Domain.ValueObjects;

namespace Domain.BoundedContexts.Orders.ValueObjects
{
    public class PaymentInfo : ValueObject
    {
        public DateTimeOffset? PayDate { get; private set; }

        public PaymentType PaymentType { get; private set; }

        public DateTimeOffset? RefundDate { get; private set; }

        public Requisites Requisites { get; private set; }

        public Money TotalPrice { get; private set; }
        public bool IsPayed => PayDate.HasValue;

        public PaymentInfo(PaymentType paymentType, Requisites requisites, Money totalPrice)
        {
            TotalPrice = totalPrice;
            PaymentType = paymentType;
            Requisites = requisites;
        }

        private PaymentInfo()
        {
        }

        /// <summary>
        /// Payed method, must called from aggregate
        /// </summary>
        /// <returns>Not nullable pay date</returns>
        public DateTimeOffset Payed(IDateTime dateTime)
        {
            if (dateTime != null)
                PayDate = dateTime.Now;

            PayDate = DateTimeOffset.Now;
            return PayDate.Value;
        }

        public void Refund()
        {
            if (!IsPayed)
                throw new OrderCannotBeRefundedException("Order not payed");
            
            RefundDate = DateTimeOffset.Now;
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return PayDate;
            yield return PaymentType;
            yield return RefundDate;
        }
    }
}