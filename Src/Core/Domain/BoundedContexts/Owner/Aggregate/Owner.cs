using Common.Abstractions;
using Common.ValueObjects;
using Domain.Abstractions;

namespace Domain.BoundedContexts.Owner.Aggregate
{
    public class Owner : DomainEntity, IAuditableEntity , IAggregateRoot
    {
        public int Id { get; private set; }
        public BankRequsites BankRequsites { get; private set; }
        public int DaysToPayment { get; private set; }
        
        public string Name => $"{GetType().Name} {Id}";
        public Owner()
        {
        }
        
        
    }
}