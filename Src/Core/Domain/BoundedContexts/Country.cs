using Domain.Enums;
using Domain.ValueObjects;

namespace Domain.BoundedContexts
{
    public class Country
    {
        public int CountryISONumber { get; set; }
        
        public string Title { get; set; }

        public Continent Continent { get; set; }

        public string PicPath { get; set; }
        
        public Vat Vat { get; set; }
    }
}