using System.Collections.Generic;
using Common.ValueObjects;

namespace Domain.BoundedContexts.Baskets.ValueObjects
{
    public class PromocodeDeactivatingResult : ValueObject
    {
        public PromocodeDeactivatingResult(bool isNeedToUpdateDiscount, IEnumerable<string> promocodesToRediscount)
        {
            IsNeedToUpdateDiscount = isNeedToUpdateDiscount;
            PromocodesToRediscount = promocodesToRediscount;
        }

        public bool IsNeedToUpdateDiscount { get; }
        public IEnumerable<string> PromocodesToRediscount { get; }

       
        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new System.NotImplementedException();
        }
    }
}