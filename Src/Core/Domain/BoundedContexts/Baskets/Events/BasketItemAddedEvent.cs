using System;
using Common.Abstractions;

namespace Domain.BoundedContexts.Baskets.Events
{
    public class BasketItemAddedEvent : IDomainEvent
    {
        public Guid BasketId { get; }
        public int ProductPlanId { get; }
        public int ProductId { get; }

        public BasketItemAddedEvent(Guid basketId, int productPlanId, int productId)
        {
            BasketId = basketId;
            ProductPlanId = productPlanId;
            ProductId = productId;
        }
    }
}