using System;
using Common.Abstractions;

namespace Domain.BoundedContexts.Baskets.Events
{
    public class BasketItemRemovedEvent: IDomainEvent
    {
        public Guid BasketId { get; }
        public int ProductPlanId { get; }
        public int ProductId { get; }

        public BasketItemRemovedEvent(Guid basketId, int productPlanId, int productId)
        {
            BasketId = basketId;
            ProductPlanId = productPlanId;
            ProductId = productId;
        }
    }
}