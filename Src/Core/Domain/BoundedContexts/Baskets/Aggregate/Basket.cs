using System;
using System.Collections.Generic;
using System.Linq;
using Common.Abstractions;
using Domain.Abstractions;
using Domain.BoundedContexts.Baskets.Events;
using Domain.BoundedContexts.Baskets.Exceptions;
using Domain.BoundedContexts.Selling.Abstractions;

namespace Domain.BoundedContexts.Baskets.Aggregate
{
    public class Basket : DomainEntity, IAggregateRoot
    {
        public Guid Id { get; set; }

        private List<ProductPlan> _basketItems = new List<ProductPlan>();
        public IReadOnlyCollection<ProductPlan> Items => _basketItems;


        public Basket(Guid customerId)
        {
            Id = customerId;
        }

        public Basket()
        {
        }


        #region Basket items actions

        public void AddBasketItem(ProductPlan productPlan)
        {
            if (productPlan == null) return;

            if (_basketItems.Any(x => x.Id == productPlan.Id))
                throw new BasketAlreadyExistItemException();


            _basketItems.Add(productPlan);

            PublishEvent(new BasketItemAddedEvent(Id, productPlan.Id, productPlan.ProductId));
        }

        public void RemoveBasketItem(int productPlanId)
        {
            var basketItemToRemove = _basketItems.Find(x => x.Id == productPlanId);
            if (basketItemToRemove == null)
                return;

            _basketItems.Remove(basketItemToRemove);

            PublishEvent(new BasketItemRemovedEvent(Id, basketItemToRemove.Id,
                basketItemToRemove.ProductId));
        }

        public void Clear()
        {
            _basketItems.Clear();
        }

        #endregion
    }
}