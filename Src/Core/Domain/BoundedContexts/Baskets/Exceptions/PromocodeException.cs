using System;

namespace Domain.BoundedContexts.Baskets.Exceptions
{
    public class PromocodeException : Exception
    {
        public PromocodeException(string message) : base(message)
        {
        }
    }
}