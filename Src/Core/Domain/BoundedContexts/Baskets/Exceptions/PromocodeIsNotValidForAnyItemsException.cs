namespace Domain.BoundedContexts.Baskets.Exceptions
{
    public class PromocodeIsNotValidForAnyItemsException : PromocodeException
    {
        public int BasketId { get; }
        public string Promocode { get; }
        
        
        public PromocodeIsNotValidForAnyItemsException(int basketId, string promocode) : base(
            $"Promocode {promocode} is not valid for any item in basket {basketId}")
        {
            BasketId = basketId;
            Promocode = promocode;
        }
    }
}