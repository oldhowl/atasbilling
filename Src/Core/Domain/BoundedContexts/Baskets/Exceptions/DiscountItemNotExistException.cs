namespace Domain.BoundedContexts.Baskets.Exceptions
{
    public class DiscountItemNotExistException : PromocodeException
    {
        public int ProductPlanId { get; }
        public string Promocode { get; }

        public DiscountItemNotExistException(int productPlanId, string promocode) 
            : base($"Discount item for plan {productPlanId} is not exist for promocode {promocode}")
        {
            ProductPlanId = productPlanId;
            Promocode = promocode;
        }
    }
}