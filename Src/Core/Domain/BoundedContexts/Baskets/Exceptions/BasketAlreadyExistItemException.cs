namespace Domain.BoundedContexts.Baskets.Exceptions
{
    public class BasketAlreadyExistItemException : BasketException
    {
        public BasketAlreadyExistItemException() : base(
            $"Product is already exist in basket")
        {
        }
    }
}