using System;

namespace Domain.BoundedContexts.Baskets.Exceptions
{
    public class BasketException : Exception
    {
        public BasketException(string message) : base(message)
        {
        }
    }
}