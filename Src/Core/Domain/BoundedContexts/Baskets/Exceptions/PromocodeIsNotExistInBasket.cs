namespace Domain.BoundedContexts.Baskets.Exceptions
{
    public class PromocodeIsNotContainInBasket : PromocodeException
    {
        public PromocodeIsNotContainInBasket(int basketId, string promocode) : base(
            $"Promocode {promocode} is not exist in basket {basketId}")
        {
            BasketId = basketId;
            Promocode = promocode;
        }

        public int BasketId { get; }
        public string Promocode { get; }
    }
}