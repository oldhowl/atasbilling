using System;
using System.Linq.Expressions;
using Domain.BoundedContexts.Baskets.Aggregate;
using Domain.BoundedContexts.Selling.Abstractions;

namespace Domain.BoundedContexts.Baskets.Specifications
{
    public class PlanOrProductNotInBasketSpecification
    {
        public static Expression<Func<ProductPlan, bool>> Build(int productId, int productPlanId)
        {
            return basketItem =>
                basketItem.Id != productPlanId;
        }
    }
}