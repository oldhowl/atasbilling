namespace Domain.BoundedContexts.Supporting.Uploads
{
    public class FileInformation
    {
        public int Id { get; set; }
        public string Path { get; set; }
        public long Size { get; set; }
        public string Extension { get; set; }
    }
}