namespace Domain.BoundedContexts.Supporting.Notifications
{
    public class CustomerAcceptedNotificationVersion
    {
        
        public int CustomerId { get; set; }
        public int NotificationVersionId { get; set; }
        
        public Customer.Aggregate.Customer Customer { get; set; }
        public NotificationVersion NotificationVersion { get; set; }
        
    }
}