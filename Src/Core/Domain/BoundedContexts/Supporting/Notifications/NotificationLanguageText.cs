using Common.Abstractions;
using Domain.Abstractions;

namespace Domain.BoundedContexts.Supporting.Notifications
{
    public class NotificationLanguageText : DomainEntity
    {
        public int Id { get; set; }
        public int LanguageId { get; set; }
        public Language Language { get; set; }
        public string AcceptButtonText { get; set; }
        public string Title { get; set; }
        public string MessageText { get; set; }

        public string Name => Title;
    }
}