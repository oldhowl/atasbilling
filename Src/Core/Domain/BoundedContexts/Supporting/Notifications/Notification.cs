using System;
using System.Collections.Generic;
using Common.Abstractions;
using Domain.Abstractions;
using Domain.Enums;

namespace Domain.BoundedContexts.Supporting.Notifications
{
    public class Notification : DomainEntity, IAuditableEntity 
    {
        public int Id { get; set; }
        public int NotificationId { get; set; }

        public ICollection<NotificationVersion> NotificationVersions { get; set; }

        public int NumberOfShowings { get; set; }

        public bool IsAcceptRequired { get; set; }

        public bool IsPublished { get; set; }

        public DateTimeOffset StartShowingDate { get; set; }
        public DateTimeOffset EndShowingDate { get; set; }

        public NotificationMessageBoxColor MessageBoxColor { get; set; }
        
        public string Name => $"{nameof(Notification)}: {NotificationId}";
    }
}