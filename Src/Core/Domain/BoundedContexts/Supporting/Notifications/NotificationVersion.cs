using System.Collections.Generic;
using Common.Abstractions;
using Domain.Abstractions;
using Domain.Enums;

namespace Domain.BoundedContexts.Supporting.Notifications
{
    public class NotificationVersion : DomainEntity
    {
        public int NotificationVersionId { get; set; }
        public NotificationType NotificationType { get; set; }
        public ICollection<NotificationLanguageText> OfferLanguageTexts { get; set; }

        public string Name => $"{nameof(NotificationVersion)}: {NotificationVersionId}";
    }
}