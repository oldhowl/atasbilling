using System;
using System.Collections.Generic;
using Common.ValueObjects;
using Domain.Abstractions;
using Domain.BoundedContexts.Orders.Aggregate;
using Domain.BoundedContexts.Selling.SubscriptionProductAggregate;
using Domain.BoundedContexts.Supporting.Notifications;
using Domain.ValueObjects;

namespace Domain.BoundedContexts.Customer.Aggregate
{
    public class Customer
    {
        public Guid Id { get; set; }
        public List<Order> _orders { get; set; } = new List<Order>();

        public IReadOnlyCollection<Order> Orders
        {
            get { return _orders; }
            private set { }
        }

        public Person Person { get; set; }

        public Customer(Guid guid, Person person)
        {
            Id = guid;
            Person = person;
        }

        private Customer()
        {
        }
    }
}