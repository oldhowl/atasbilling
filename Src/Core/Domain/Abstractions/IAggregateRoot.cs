namespace Domain.Abstractions
{
    /// <summary>
    /// Aggregate root marker
    /// </summary>
    /// https://docs.microsoft.com/ru-ru/dotnet/architecture/microservices/microservice-ddd-cqrs-patterns/net-core-microservice-domain-model#implement-domain-entities-as-poco-classes
    public interface IAggregateRoot
    {
        
    }
}