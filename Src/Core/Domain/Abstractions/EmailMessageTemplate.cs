using System.Collections.Generic;
using Domain.Enums;

namespace Domain.Abstractions
{
    public class EmailMessageTemplate
    {
        public int EmailMessageTemplateId { get; set; }
        public string SenderEmail { get; set; }
        public int StoreDays { get; set; }
        public EmailPublishStatus PublishStatus { get; set; }
        public EmailTemplateType TemplateType { get; set; }

        public ICollection<EmailMessgeTranslation> EmailMessgeTranslations { get; set; }

    }
}