using Domain.BoundedContexts;

namespace Domain.Abstractions
{
    public class EmailMessgeTranslation
    {
        public int EmailMessgeTranslationId { get; set; }
        
        public int LanguageId { get; set; }
        public Language Language { get; set; }

        public string Subject { get; set; }
        public string Text { get; set; }
        public string From { get; set; }
    }
}