using Domain.Enums;

namespace Domain.Abstractions
{
    public class CustomizationField
    {
        /// <summary>
        /// Значение поля
        /// </summary>
        public object Value { get; set; }

        /// <summary>
        /// Техническое наименование для основного клиента
        /// </summary>
        public string Name { get; set; }


        /// <summary>
        /// Тип поля
        /// </summary>
        public CustomFieldType Type { get; set; }
    }
}