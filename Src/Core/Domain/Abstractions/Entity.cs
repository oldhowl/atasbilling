using System.Collections.Generic;
using Common.Abstractions;
using Domain.BoundedContexts.Supporting.Uploads;
using Domain.ValueObjects;

namespace Domain.Abstractions
{
    public abstract class Entity : DomainEntity, IAuditableEntity
    {
        public int Id { get; set; }
        public Address Address { get; set; }

        public string UserEmail { get; set; }

        public ICollection<FileInformation> Files { get; set; }

        public EntityRequisites Requisites { get; set; }
        public abstract string FullName();

        public abstract string FullAddress();

        public abstract string Name { get; }
    }
}