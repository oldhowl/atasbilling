using System.Threading.Tasks;
using Common.Abstractions;

namespace Domain.EventDispatcher
{
    public interface IDomainEventDispatcher
    {
        Task Dispatch(IDomainEvent devent);

    }
}