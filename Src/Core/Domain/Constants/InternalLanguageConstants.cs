namespace Domain.Constants
{
    public class InternalLanguageConstants
    {
        public class Common
        {
            public const string ObjectChanged = "Объект изменен";
        }
        
        public class Products
        {
            
        }
        
        public class Partners
        {
            public const string PartnerRequisitesWasChanged = "Реквизиты партнера были изменины";
        }
    }
}