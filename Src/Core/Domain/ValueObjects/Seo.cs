namespace Domain.ValueObjects
{
    public class Seo
    {
        public string GoogleTagEventCategory { get; set; }
        public string GoogleTagEventClick { get; set; }
        
        
    }
}