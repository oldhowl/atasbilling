using System;
using System.Collections.Generic;
using Common.ValueObjects;

namespace Domain.ValueObjects
{
    public class Person : ValueObject
    {
        

        public string Name { get; private set; }
        public string Surname { get; private set; }
        public DateTime Birthdate { get; private set; }
        public Address Address { get; private set; }
        public Contacts Contacts { get; private set; }
        
        public Person(string name, string surname, DateTime birthdate, Address address, Contacts contacts)
        {
            Name = name;
            Surname = surname;
            Birthdate = birthdate;
            Address = address;
            Contacts = contacts;
        }
        
        private Person(){}
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Name;
            yield return Surname;
            yield return Birthdate;
            yield return Address;
            yield return Contacts;
        }
    }
}