using System.Collections.Generic;
using Common.ValueObjects;

namespace Domain.ValueObjects
{
    public class Vat : ValueObject
    {
        public decimal Rate { get; set; } 

        public decimal CalculateVat(decimal price)
        {
            return price / 100 * Rate;
        }

        public Vat(decimal rate)
        {
            Rate = rate;
        }
        
        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Rate;
        }
    }
}