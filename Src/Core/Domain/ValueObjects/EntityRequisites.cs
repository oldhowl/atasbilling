namespace Domain.ValueObjects
{
    public class EntityRequisites
    {
        public int ApartmentNumber { get; set; }
        public int TaxpayerNumber { get; set; }
        public int VATpayerNumber { get; set; }
        public bool IsNotRegistredForVAT { get; set; }
        public string CheckingAccount { get; set; }
        public string BankName { get; set; }
        public string BankAddress { get; set; }
        public string BIC { get; set; }
        public string Swift { get; set; }
        public string CorrespondentBankData { get; set; }
        public string PayPalAccountName { get; set; }
    }
}