using Domain.Enums;

namespace Domain.ValueObjects
{
    public class NotificationSettings
    {
        public bool NotificationsEnabled { get; set; }
        public MailLanguage MailLanguage { get; set; }
        public bool SystemLettersEnabled { get; set; }

        public bool PlatformUpdatesEnabled { get; set; }
        public bool NewBlogArticlesEnabled { get; set; }
        public bool NewVideosEnabled { get; set; }

        public bool PromotionsEnabled { get; set; }

        public bool WebinarsEnabled { get; set; }

        public NotificationSettings(MailLanguage mailLanguage)
        {
            MailLanguage = mailLanguage;
            NotificationsEnabled = true;
            SystemLettersEnabled = true;
            PlatformUpdatesEnabled = true;
            NewBlogArticlesEnabled = true;
            NewVideosEnabled = true;
            PromotionsEnabled = true;
            WebinarsEnabled = true;
        }


        private NotificationSettings()
        {
        }
    }
}