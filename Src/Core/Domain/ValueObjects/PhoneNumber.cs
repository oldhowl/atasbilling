using System.Collections.Generic;
using Common.ValueObjects;

namespace Domain.ValueObjects
{
    public class PhoneNumber : ValueObject
    {
        public int Code { get; private set; }
        public string Phone { get; private set; }

        public PhoneNumber(int code, string phone)
        {
            Code = code;
            Phone = phone;
        }

        private PhoneNumber()
        {
            
        }

        public override string ToString()
        {
            return $"+{Code}{Phone}";
        }

        protected override IEnumerable<object> GetAtomicValues()
        {
            throw new System.NotImplementedException();
        }
    }
}