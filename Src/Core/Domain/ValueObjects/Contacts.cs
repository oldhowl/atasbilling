namespace Domain.ValueObjects
{
    public class Contacts
    {
        public string WebsiteUrl { get; private set; }
        public string WebsiteDescription { get; private set; }
        public int WebsiteSubscribersNumber { get; private set; }
        public string SkypeUsername { get; private set; }
        public PhoneNumber PhoneNumber { get; private set; }

        private Contacts()
        {
            
        }
        public Contacts(string websiteUrl, string websiteDescription, int websiteSubscribersNumber, string skypeUsername, PhoneNumber phoneNumber)
        {
            WebsiteUrl = websiteUrl;
            WebsiteDescription = websiteDescription;
            WebsiteSubscribersNumber = websiteSubscribersNumber;
            SkypeUsername = skypeUsername;
            PhoneNumber = phoneNumber;
        }
    }
}