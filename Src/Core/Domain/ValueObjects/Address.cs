using System.Collections.Generic;
using Common.ValueObjects;

namespace Domain.ValueObjects
{
    public class Address : ValueObject
    {
        public string Country { get; private set; }
        public string City { get; private set; }
        public string Region { get; private set; }
        public string Street { get; private set; }
        public string HouseNumber { get; private set; }
        public string AppartmentOffice { get; private set; }
        public int ZipCode { get; private set; }

        public Address(string country, string city, string region, string street, string houseNumber, string appartmentOffice, int zipCode)
        {
            Country = country;
            City = city;
            Region = region;
            Street = street;
            HouseNumber = houseNumber;
            AppartmentOffice = appartmentOffice;
            ZipCode = zipCode;
        }
        
        private Address(){}

        protected override IEnumerable<object> GetAtomicValues()
        {
            yield return Country;
            yield return City;
            yield return Region;
            yield return Street;
            yield return ZipCode;
            yield return HouseNumber;
            yield return AppartmentOffice;
        }
    }
}