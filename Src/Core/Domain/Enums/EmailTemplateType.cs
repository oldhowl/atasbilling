namespace Domain.Enums
{
    public enum EmailTemplateType
    {
        CustomProduct,
        UserRegisration,
        PartnerRegistration,
        WithdrawalSuccessed,
        WithdrawalCanceled,
        AccrualToBalance,
        PasswordRecovery,
        DemoRegistration,
        PaidVersionRegistration,
        PaypalPaying,
        ConfirmationOfSavingPaymentDetails,
        BankPaying,
        Refound,
        PartnershipDetailsChangeRequestNotification,
        PartnershipConfirmation,
        PartnershipCanceling
    }
}