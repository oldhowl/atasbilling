namespace Domain.Enums
{
    public enum SubscriptionType
    {
        Payed,
        Demo,
        ExtendedDemo,
        Study,
        Live
    }
}