namespace Domain.Enums
{
    public enum PaymentType
    {
        Paypal,
        Bank,
        Card
    }
}