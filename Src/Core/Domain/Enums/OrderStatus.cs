namespace Domain.Enums
{
    public enum OrderStatus
    {
        PaymentRefund,
        WaitingPayment,
        Payed
    }
}