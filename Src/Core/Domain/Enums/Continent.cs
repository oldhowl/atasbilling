namespace Domain.Enums
{
    public enum Continent
    {
        AF,
        NA,
        OC,
        AN,
        AS,
        EU,
        SA
    }
}