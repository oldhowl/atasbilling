namespace Domain.Enums
{
    public enum NotificationMessageBoxColor
    {
        White = 0x000000,
        Blue = 0x00B3FF,
        Red = 0xFF0000,
        Orange = 0xFF8C00,
        Green = 008000
    }
}