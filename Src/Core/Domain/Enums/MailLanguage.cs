namespace Domain.Enums
{
    public enum MailLanguage
    {
        Ru,
        En
    }
}