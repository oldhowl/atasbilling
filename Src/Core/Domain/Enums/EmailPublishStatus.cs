namespace Domain.Enums
{
    public enum EmailPublishStatus
    {
        Published,
        Hidden
    }
}