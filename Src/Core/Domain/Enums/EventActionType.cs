namespace Domain.Enums
{
    public enum EventActionType
    {
        Created,
        Modified,
        Deleted
    }
}