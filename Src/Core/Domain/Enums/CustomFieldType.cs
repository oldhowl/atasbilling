namespace Domain.Enums
{
    public enum CustomFieldType
    {
        Bool,
        Int,
        String,
        Double,
        DateTime
    }
}